<?php

Route::group(['namespace' => 'Freshfield\Core\Http\Controllers', 'middleware' => ['web', 'checkAuthentication', 'checkAccess', 'checkScheduledCommands']], function() {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/home', function() { redirect()->route('dashboard'); })->name('home');
    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::post('/profile/save', 'ProfileController@save')->name('profile-save');
    Route::get('/success', 'FallbackController@success')->name('success');
    Route::get('/error', 'FallbackController@error')->name('error');
    Route::get('/{type}/list', 'GeneralController@index')->name('list');
    Route::get('/{type}/create', 'GeneralController@create')->name('create');
    Route::get('/{type}/trash', 'GeneralController@trash')->name('trash');
    Route::get('/{type}/restore/{id}', 'GeneralController@restore')->name('restore');
    Route::get('/{type}/{id}/edit', 'GeneralController@edit')->name('edit');
    Route::post('/{type}/save', 'GeneralController@save')->name('save');
    Route::get('/{type}/{id}/delete', 'GeneralController@delete')->name('delete');
    Route::get('/settings', 'GeneralController@settings')->name('settings');
});

Route::group(['namespace' => 'Freshfield\Core\Http\Controllers', 'middleware' => ['web']], function() {
    Route::get('/login', 'LoginController@index')->name('login');
    Route::post('/login', 'LoginController@login')->name('login-post');
    Route::post('/logout', 'LoginController@logout')->name('logout');
});

Route::fallback(function () {
    abort(404, 'Route not found');
});