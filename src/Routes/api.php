<?php

Route::group(['middleware' => 'api', 'namespace' => 'Freshfield\Core\Http\Controllers', 'prefix' => 'api'], function() {
    Route::get('/block/{type}/{id}/{name}', 'ApiController@blocks');
    Route::get('/{type}', 'ApiController@get');
    Route::get('/{type}/{id}', 'ApiController@get');
});