<?php

Artisan::command('api:generate', function() {
    $key = substr(str_shuffle("aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ0123456789"), 0, 20);
    \Illuminate\Support\Facades\DB::table('api_keys')->insert(['key' => $key]);
    dump("Your key is: $key");
});

Artisan::command('clean_deletes', function() {
    $time = date('Y-m-d H:i:s', strtotime('-30 days'));
    $forms = config('freshfield')['forms'];
    foreach($forms as $form) {
        \Illuminate\Support\Facades\DB::table($form->getTable())->where('deleted_at', '<', $time)->delete();
    }
});

//Artisan::command('routes:clear', function() {
//
//    // Option 1. Empty the laravel.log file, OR
//    exec('echo "" > ' . storage_path('routes/web.php'));
//
//    $this->comment('Routes have been cleared!');
//
//})->describe('Clear log files');