<?php

namespace Freshfield\Core\Base\Lists;

class BaseList
{
    /**
     * @return string
     */
    public function getTitle()
    {
        return '';
    }

    /**
     * @return string|null
     */
    public function getTable()
    {
        return null;
    }

    /**
     * [
     *  '{name}' => '{table}.{filter}.{key}',
     * ]
     *
     * @return string[]
     */
    public function getForeign()
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getVisible()
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getInvisible()
    {
        return [
            'id',
        ];
    }

    /**
     * @return string[]
     */
    public function getLabels()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getIndex()
    {
        return 'id';
    }

    /**
     * @return string[]
     */
    public function getSort()
    {
        return [
            'id' => 'ASC',
        ];
    }

    /**
     * @return string
     */
    public function getFilter()
    {
        return 'id';
    }

    /**
     * @return string[]
     */
    public function getMobileVisible()
    {
        return [];
    }
}