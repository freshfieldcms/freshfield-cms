<?php

namespace Freshfield\Core\Base\Role;


class BaseRole
{
    /**
     * An array with all the permissions. Example:
     *  [
     *      'users' => ['edit', 'delete'],
     *      'navigation' => ['edit', 'delete'],
     *  ]
     *
     * @return array
     */
    public function getPermissions()
    {
        return [];
    }
}