<?php

namespace Freshfield\Core\Base\Block;

class BaseBlock
{
    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getIndex()
    {
        return 'id';
    }

    /**
     * @return string[]
     */
    public function getItems()
    {
        return ['id'];
    }

    /**
     * @return string[]
     */
    public function getBlocks()
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getTypes()
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getLabels()
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getInvisible()
    {
        return [
            'id',
            'type',
        ];
    }

    /**
     * @return string[]
     */
    public function getRequired()
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getForeign()
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getIcons()
    {
        return [];
    }
}