<?php

namespace Freshfield\Core\Base\Form;

class BaseForm
{
    /**
     * @return string
     */
    public function getTable()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return '';
    }

    /**
     * @return string[]
     */
    public function getItems()
    {
        return [
            'id'
        ];
    }

    /**
     * @return string[]
     */
    public function getTypes()
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    /**
     * @return string[]
     */
    public function getVisible()
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getInvisible()
    {
        return [
            'id'
        ];
    }

    /**
     * @return string[]
     */
    public function getRequired()
    {
        return [];
    }

    /**
     * @return string[]
     */
    public function getValidation()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getIndex()
    {
        return 'id';
    }

    /**
     * @return string[]
     */
    public function getSort()
    {
        return ['id DESC'];
    }

    /**
     * [
     *  '{name}' => '{table}.{filter}.{key}',
     * ]
     *
     * @return string[]
     */
    public function getForeign()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getCustomView()
    {
        return '';
    }

}