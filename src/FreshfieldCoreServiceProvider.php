<?php

namespace Freshfield\Core;

use Freshfield\Core\Helpers\Helpers;
use Freshfield\Core\Http\Middleware\CheckAccess;
use Freshfield\Core\Http\Middleware\CheckAuthentication;
use Freshfield\Core\Http\Middleware\CheckScheduledCommands;
use Illuminate\Support\Facades\Schema;

class FreshfieldCoreServiceProvider extends \Illuminate\Support\ServiceProvider
{
//    use Helpers;
    public function boot()
    {
        Schema::defaultStringLength(191);

        app('router')->aliasMiddleware('checkScheduledCommands', CheckScheduledCommands::class);
        app('router')->aliasMiddleware('checkAuthentication', CheckAuthentication::class);
        app('router')->aliasMiddleware('checkAccess', CheckAccess::class);

        $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');
        $this->loadRoutesFrom(__DIR__. '/Routes/api.php');
        $this->loadRoutesFrom(__DIR__. '/Routes/console.php');
        $this->loadMigrationsFrom(__DIR__. '/Migrations');

        $this->loadViewsFrom(__DIR__ . '/Resources/Views', 'cms');
        $this->publishes([
            __DIR__.'/Resources/Assets/js/' => public_path('/js/'),
            __DIR__.'/Resources/Assets/css/' => public_path('/css/'),
            __DIR__.'/Resources/Assets/fonts/' => public_path('/fonts/'),
            __DIR__.'/Resources/Assets/img/' => public_path('/img/'),
            __DIR__.'/Resources/Assets/svg/' => public_path('/svg/'),
        ], 'public');

        $this->publishes([
            __DIR__ . '/Config/freshfield.php' => config_path('freshfield.php')
        ], 'config');

        if(isset(config('freshfield')['commands'])) {
            $this->commands(config('freshfield')['commands']);
        }

        $this->app->alias(Helpers::class, 'Freshfield');
    }

    public function register()
    {

    }
}