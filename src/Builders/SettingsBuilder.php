<?php

namespace Freshfield\Core\Builders;


use Freshfield\Core\Render\BaseRender;

class SettingsBuilder extends BaseRender
{
    protected $defaultSettings = [
        'theme' => [
            'label' => 'Which theme do you prefer?',
            'options' => [
                'Dark',
                'Light',
            ],
        ],
    ];

    public function index()
    {
        return view('cms::settings',  $this->__getReturnData());
    }

    public function getSettings()
    {
        return $this->defaultSettings;
    }

    private function __getReturnData() {
        return [
            'routes' => $this->getRoutes(),
            'settings' => $this->getSettings(),
        ];
    }
}