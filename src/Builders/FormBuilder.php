<?php

namespace Freshfield\Core\Builders;

use Freshfield\Core\Auth\Access;
use Freshfield\Core\Base\Form\BaseForm;
use Freshfield\Core\Processing\Delete;
use Freshfield\Core\Processing\Restore;
use Freshfield\Core\Processing\Retrieve;
use Freshfield\Core\Processing\Save;
use Freshfield\Core\Render\BaseRender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class FormBuilder extends BaseRender
{
    /**
     * @param string $route
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function render(string $route, $id = null)
    {
        /**
         * @var $form BaseForm
         */
        $form = Retrieve::form($route);

        if (!empty($form->getCustomView())) {
            return view($form->getCustomView(), $this->__getReturnData($form, $id, $route));
        }

        return view('cms::form', $this->__getReturnData($form, $id, $route));
    }

    private function __getBlocksInfo($types)
    {
        $output = [];
        foreach($types as $name => $type) {
            if ($type === 'blocks') $output[$name] = Retrieve::getBlockInfo($name);
        }
        return $output;
    }

    /**
     * @param BaseForm $form
     * @param $id
     * @param string $type
     * @return array
     * @throws \Exception
     */
    private function __getReturnData(BaseForm $form, $id, string $type)
    {
        return [
            'items' => $this->getItems($form),
            'invisible' => $form->getInvisible(),
            'labels' => $form->getLabels(),
            'types' => $form->getTypes(),
            'selectData' => Retrieve::foreignKey($form),
            'values' => !empty($id) ? Retrieve::retrieveFormData($form, $id) : null,
            'type' => $type,
            'blocks' => $this->__getBlocksInfo($form->getTypes()),
            'routes' => $this->getRoutes(),
            'title' => $form->getTitle(),
            'required' => $form->getRequired(),
            'back_url' => $this->getBackUrl($type),
        ];
    }

    public function getItems(BaseForm $form)
    {
        $items = $form->getItems();

        foreach($items as $key => $item) {
            if (is_array($item) === false && str_contains($item, '.'))
            {
                $parts = explode('.', $item);
                if (!isset($items[$parts[0]])) $items[$parts[0]] = [];
                $items[$parts[0]][] = $parts[1];
                unset($items[$key]);
            }
        }

        return $items;
    }

    /**
     * @param Request $request
     * @param string $type
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function save(Request $request, string $type)
    {
        $isSaved = Save::save($request, $type);

        if ($isSaved) {
            session(['success' => true]);
        } else {
            session(['error' => true]);
        }

        return redirect(URL::previous());
    }

    /**
     * @param string $type
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function delete(string $type, $id)
    {
        $isDeleted = Delete::delete($type, $id);

        if ($isDeleted) {
            session(['success' => true]);
        } else {
            session(['error' => true]);
        }

        return redirect(URL::previous());
    }

    /**
     * @param string $type
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function restore(string $type, $id)
    {
        $isRestored = Restore::restore($type, $id);

        if ($isRestored) {
            session(['success' => true]);
        } else {
            session(['error' => true]);
        }

        return redirect(URL::previous());
    }

    /**
     * @param string $type
     * @return string
     */
    public function getBackUrl(string $type)
    {
        if (isset(config('freshfield')['lists'][$type])) return route('list', ['type' => $type]);
        else return route('dashboard');
    }
}