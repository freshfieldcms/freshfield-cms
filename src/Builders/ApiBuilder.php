<?php

namespace Freshfield\Core\Builders;

use Freshfield\Core\Api\ApiRetrieve;
use Freshfield\Core\Base\Form\BaseForm;
use Freshfield\Core\Processing\Retrieve;
use Illuminate\Support\Facades\DB;

class ApiBuilder
{
    protected $type;
    /**
     * @var BaseForm
     */
    protected $form;
    protected $id;

    public function __construct($type, $id = null)
    {
        $this->type = $type;
        $this->form = Retrieve::form($type);
        $this->id = $id;
    }

    public static function validateKey($key)
    {
        if (!DB::table('api_keys')->select('key')->where('key', $key)->first()) {
            return false;
        }
        return true;
    }

    public function get()
    {
        return ApiRetrieve::getData($this->form, $this->id);
    }
}