<?php

namespace Freshfield\Core\Builders;

use Freshfield\Core\Auth\Access;
use Freshfield\Core\Base\Lists\BaseList;
use Freshfield\Core\Processing\Retrieve;
use Freshfield\Core\Render\BaseRender;
use Illuminate\Support\Facades\DB;

class ListBuilder extends BaseRender
{
    /**
     * @param string $route
     * @param string $filter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function render(string $route, string $filter = null)
    {
        $list = Retrieve::list($route);

        return view('cms::list', $this->__getReturnData($list, $route, $filter));
    }

    /**
     * @param BaseList $list
     * @param string $type
     * @param string $filter
     * @return array
     */
    private function __getReturnData(BaseList $list, string $type, string $filter = null)
    {
        return [
            'visible' => $list->getVisible(),
            'mobile' => $list->getMobileVisible(),
            'invisible' => $list->getInvisible(),
            'labels' => $list->getLabels(),
            'index' => $list->getIndex(),
            'items' => Retrieve::retrieveListData($list->getTable(), $list->getSort(), !empty($filter) ? ['index' => $list->getFilter(), 'value' => $filter] : []),
            'type' => $type,
            'routes' => $this->getRoutes(),
            'title' => $list->getTitle(),
        ];
    }

    /**
     * @param string $route
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trash(string $route)
    {
        /**
         * @var $list BaseList
         */
        $list = Retrieve::list($route);
        return view('cms::list', $this->__getTrashReturnData($list, $route));
    }

    /**
     * @param BaseList $list
     * @param string $type
     * @return array
     */
    private function __getTrashReturnData(BaseList $list, string $type)
    {
        return [
            'visible' => $list->getVisible(),
            'mobile' => $list->getMobileVisible(),
            'invisible' => $list->getInvisible(),
            'labels' => $list->getLabels(),
            'index' => $list->getIndex(),
            'items' => Retrieve::retrieveTrashListData($list->getTable(), $list->getSort()),
            'type' => $type,
            'routes' => $this->getRoutes(),
            'title' => $list->getTitle().' Trash',
            'trash' => true,
        ];
    }
}