<?php

namespace Freshfield\Core\Blocks\Content;

use Freshfield\Core\Base\Block\BaseBlock;

class ContentBlock extends BaseBlock
{
    public function getTitle()
    {
        return 'Content Block';
    }

    public function getItems()
    {
        return [
            'id',
            'type',
            'title',
            'text',
            'image_id',
            'video_url',
            'created_at',
            'updated_at',
            'deleted_at'
        ];
    }

    public function getTypes()
    {
        return [
            'text' => 'tinymce',
            'image_id' => 'image'
        ];
    }

    public function getLabels()
    {
        return [
            'type' => 'Type',
            'title' => 'Title',
            'text' => 'Text',
            'image_id' => 'Image',
            'video_url' => 'Video'
        ];
    }

    public function getVisible()
    {
        return [
            'type',
            'title',
            'text',
            'image_id',
            'video_url'
        ];
    }

    public function getInvisible()
    {
        return [
            'id'
        ];
    }

    public function getTable()
    {
        return 'blocks_content';
    }

    public function getForeign()
    {
        return [
            'image_id' => 'images.id.source'
        ];
    }

}