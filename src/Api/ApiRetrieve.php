<?php

namespace Freshfield\Core\Api;


use Freshfield\Core\Base\Form\BaseForm;
use Freshfield\Core\Processing\Retrieve;
use Illuminate\Support\Facades\DB;

class ApiRetrieve
{
    /**
     * Get's the form and the results of the primary query
     * Returns the primary query but with all the foreign key data
     *
     * @param BaseForm $form
     * @param $mainResults
     * @return mixed
     */
    static function __getForeign(BaseForm $form, $mainResults) {
        $output = $mainResults;

        foreach ($form->getForeign() as $key => $value) {
            $foreign = explode('.', $value);
            /**
             * @var $foreignForm BaseForm
             */
            $foreignForm = Retrieve::form($foreign[0]);

            $data = DB::table($foreignForm->getTable())->select($foreign[2])->where($foreign[1], $output->{$key})->whereNull('deleted_at')->first();
            if (!empty($data->{$foreign[2]})) {
                $output->{$key} = $data->{$foreign[2]};
            } else {
                $output->{$key} = null;
            }
        }

        return $output;
    }

    /**
     * Get's the form and the results of the primary query
     * Returns the primary query but each item with type json has been converted from plain text to json
     *
     * @param $types
     * @param $mainResults
     * @return mixed
     */
    static function __getJson($types, $mainResults) {
        foreach($types as $item => $type) {
            if ($type === 'json') $mainResults->{$item} = json_decode($mainResults->{$item});
        }

        return $mainResults;
    }

    /**
     * Get the linked Blocks data
     *
     * @param $types
     * @param $mainResults
     * @return mixed
     */
    static function __getBlock($types, $mainResults) {
        foreach($types as $name => $type) {
            if ($type === 'blocks') {
                if (str_contains($mainResults->{$name}, ',')) $ids = explode(',', $mainResults->{$name});
                else $ids = [$mainResults->{$name}];

                $block = Retrieve::block($name);
                $output = [];
                foreach($ids as $id) {
                    $output[] = DB::table($block->getTable())->select($block->getItems())->where('id', $id)->whereNull('deleted_at')->first();
                }

                $mainResults->{$name} = $output;
            }
        }

        return $mainResults;
    }

    /**
     * Get image with source
     *
     * @param $types
     * @param $mainResults
     * @return mixed
     */
    static function __getImageLink($types, $mainResults) {
        foreach($types as $name => $type) {
            if ($type === 'image') {
                $mainResults->{$name} = !empty($mainResults->{$name}) ? env('APP_URL').'/storage/images/'.$mainResults->{$name} : "";
            }
        }

        return $mainResults;
    }

    /**
     * @param BaseForm $form
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|mixed|null|object
     */
    static function getData($form, $id)
    {
        $orderBy = $form->getSort();
        $items = Retrieve::filterItems($form->getItems());
        $query = DB::table($form->getTable())->select($items)->where($form->getIndex(), $id);

        foreach($orderBy as $item) {
            $order = explode(' ', $item);
            $query->orderBy($order[0], $order[1]);
        }

        $results = $query->first();

        if (!empty($form->getForeign())) {
            $results = self::__getForeign($form, $results);
        }
        if (!empty($form->getTypes())) {
            $types = $form->getTypes();
            if (in_array('blocks', $types)) {
                $results = self::__getBlock($types, $results);
            }
            if (in_array('json', $types)) {
                $results = self::__getJson($types, $results);
            }
            if (in_array('image', $types)) {
                $results = self::__getImageLink($types, $results);
            }
        }

        return $results;
    }
}