<?php

namespace Freshfield\Core\Auth;


use Illuminate\Support\Facades\Auth;

class RetrieveValues
{
    static function email()
    {
        return Auth::user();
    }

    static function id()
    {
        return Auth::id();
    }
}