<?php

namespace Freshfield\Core\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Access
{
    static $defaultRoutes = [
        'profile',
        '/',
        'settings',
        'restore',
        'success',
    ];

    /**
     * @param string $route
     */
    static function check(string $route)
    {
        if (!in_array($route, self::$defaultRoutes) && !str_contains($route, self::$defaultRoutes)) {
            $query = DB::table('cms_routes')->select('id')->where('url', 'like', '/' . $route . '%')->first();
            if (empty($query)) {
                abort(404);
            }

            $user = DB::table('users')->select('role_id')->where('id', Auth::id())->first();
            $role = DB::table('cms_roles')->select('permissions')->where('id', $user->role_id)->first();

            if ($role->permissions !== '*') {
                $roles = explode(',', $role->permissions);
                if (!in_array($query->id, $roles) && !in_array($query->parentId, $roles)) {
                    abort(403, 'unauthorized action');
                }
            }
        }
    }
}