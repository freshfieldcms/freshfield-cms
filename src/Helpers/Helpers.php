<?php

namespace Freshfield\Core\Helpers;

use Freshfield\Core\Auth\RetrieveValues;

class Helpers
{
    static function auth()
    {
        return new RetrieveValues();
    }

    static function checkIfValueInside($haystack, $needle, $key, $returnKey)
    {
        foreach($haystack as $item) {
            if ($item[$key] === $needle) {
                return $item;
            }
        }

        return '';
    }
}