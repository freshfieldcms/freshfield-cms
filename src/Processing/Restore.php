<?php

namespace Freshfield\Core\Processing;


use Freshfield\Core\Base\Form\BaseForm;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Restore
{
    /**
     * Restore deleted item
     *
     * @param $type
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    static function restore($type, $id)
    {
        /**
         * @var $form BaseForm
         */
        $form = Retrieve::form($type);
        $data = DB::table($form->getTable())->select('*')->where('id', $id)->first();
        foreach($form->getTypes() as $key => $field) {
            if ($field === 'image') {
                Restore::image($data->{$key});
            } else if ($field === 'blocks') {
                Restore::blocks($data->{$key});
            }
        }
        if (DB::table($form->getTable())->where('id', $id)->update(['deleted_at' => NULL])) {
            return redirect()->route('list', ['type' => $type]);
        } else {
            return redirect()->route('error');
        }
    }

    /**
     * Restore the blocks that are linked to the item
     *
     * @param $values
     */
    static function blocks($values)
    {
        $ids = explode(',', $values);
        foreach($ids as $id) {
            $block = DB::table('blocks')->select('*')->where('id', $id)->first();
            if($block->type === 'image') Restore::image($block->image);
            DB::table('blocks')->where('id', $id)->update(['deleted_at' => NULL]);
        }
    }

    /**
     * Restore the image that is linked to the item
     *
     * @param $image
     */
    static function image($image)
    {
        if ($image == 1 || empty($image)) return;
        $imageSource = DB::table('images')->select('source')->where('id', $image)->first();
        if(empty($imageSource) || !Storage::exists('public/deleted/'.$imageSource->source)) return;
        Storage::move('public/deleted/'.$imageSource->source, 'public/images/'.$imageSource->source);
        if (Storage::exists('public/images/'.$imageSource->source) && Storage::exists('public/deleted/'.$imageSource->source)) {
            Storage::delete('public/deleted/'.$imageSource->source);
        }
        $time = date('Y-m-d H:i:s');
        DB::table('images')->where('id', $image)->update(['updated_at' => $time, 'deleted_at' => NULL]);
    }
}