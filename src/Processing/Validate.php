<?php

namespace Freshfield\Core\Processing;

use Illuminate\Support\Facades\Validator;

class Validate
{
    /**
     * @param $rules
     * @param $data
     * @throws \Exception
     */
    static function validate($rules, $data)
    {
        Validator::make($data, $rules)->validate();
    }
}