<?php

namespace Freshfield\Core\Processing;

use Freshfield\Core\Base\Block\BaseBlock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class Save
{
    /**
     * Check if the array contains the required values
     *
     * @param array $array
     * @param array $required
     * @return bool
     */
    static private function __checkIfEmptyArray(array $array, array $required = [])
    {
        $empty = false;
        foreach ($array as $key => $item) {
            if (in_array($key, $required) && empty($item)) $empty = true;
        }

        return $empty;
    }

    /**
     * Save data
     *
     * @param Request $request
     * @param string $type
     * @return bool|int
     * @throws \Exception
     */
    static function save(Request $request, string $type)
    {
        \Tinify\setKey("sRlRd1eFt8SPOCAVR3zUcAMc08k8jqsK");
        $form = Retrieve::form($type);

        Validate::validate($form->getValidation(), $request->all());

        $insert = [];
        $blockTypes = $form->getTypes();
        $blockItems = $form->getItems();
        $values = $request->all();

        $time = date('Y-m-d H:i:s');

        if (in_array('json', $blockTypes)) {
            foreach($blockTypes as $name => $blockType) {
                if ($blockType === 'json') {
                    $temp = [];
                    foreach($blockItems as $blockKey => $blockItem) {
                        if (!is_array($blockItem) && $name === explode('.', $blockItem)[0]) {
                            $temp[explode('.', $blockItem)[1]] = $values[str_replace('.', '_', $blockItem)];
                        }
                    }
                    $insert[$name] = json_encode($temp);
                    unset($blockTypes[$name]);
                }
            }
        }

        foreach ($blockItems as $blockItemName => $blockItem) {
            if (is_array($blockItem)) {
                if (!$request->has($blockItemName)) continue;

                if (!empty($blockTypes[$blockItemName])) {
                    if ($blockTypes[$blockItemName] === 'password') {
                        $insert[$blockItemName] = Hash::make($request->get($blockItemName));
                    } else if ($blockTypes[$blockItemName] === 'blocks') {
                        $insert[$blockItemName] = self::block($request, $blockItemName);
                    } else if ($blockTypes[$blockItemName] === 'boolean') {
                        $insert[$blockItemName] = $request->has($blockItemName);
                    } else if ($blockTypes[$blockItemName] === 'image') {
                        $insert[$blockItemName] = self::image($request->file($blockItemName));
                    } else {
                        $insert[$blockItemName] = $request->get($blockItemName);
                    }
                } else {
                    $insert[$blockItemName] = $request->get($blockItemName);
                }
            } else {
                if (!$request->has($blockItem)) continue;

                if (!empty($blockTypes[$blockItem])) {
                    if ($blockTypes[$blockItem] === 'password') {
                        $insert[$blockItem] = Hash::make($request->get($blockItem));
                    } else if ($blockTypes[$blockItem] === 'blocks') {
                        $insert[$blockItem] = self::block($request, $blockItem);
                    } else if ($blockTypes[$blockItem] === 'boolean') {
                        $insert[$blockItem] = $request->has($blockItem);
                    } else if ($blockTypes[$blockItem] === 'image') {
                        $insert[$blockItem] = self::image($request->file($blockItem));
                    } else {
                        $insert[$blockItem] = $request->get($blockItem);
                    }
                } else {
                    $insert[$blockItem] = $request->get($blockItem);
                }
            }
        }

        if (empty($request->get('id'))) {
            $insert['created_at'] = $time;
        }
        $insert['updated_at'] = $time;

        if ($request->has('id') && !empty($request->get('id'))) {
            return DB::table($form->getTable())->where('id', $request->get('id'))->update($insert);
        } else {
            return DB::table($form->getTable())->insert($insert);
        }
    }

    /**
     * Save all the Blocks
     *
     * @param Request $request
     * @param string $blockName
     * @return int|string
     */
    static function block(Request $request, string $blockName)
    {
        $blocks = $request->get($blockName);
        $files = $request->file($blockName);

        $oldBlocks = !empty($blocks['old_blocks']) ? explode(',', $blocks['old_blocks']) : [];

        $block = Retrieve::block($blockName);

        /**
         * @var BaseBlock $block
         */
        $block = new $block;
        $table = $block->getTable();

        foreach($blocks as $index => $item) {
            if ($index === 'old_blocks' ||  self::__checkIfEmptyArray($item, $block->getRequired())) continue;
            if (isset($item['id']) && in_array($item['id'], $oldBlocks)) {
                unset($oldBlocks[array_search($item['id'], $oldBlocks)]);

                if (in_array('image', $block->getTypes())) {
                    foreach ($block->getTypes() as $name => $type) {
                        if ($type !== 'image') continue;
                        //Check if there are files inside this block
                        if (!empty($files[$item['id']])) {
                            if (!empty($item[$name . '_old'])) {
                                //Delete old image
                                Delete::image($block[$name . '_old']);
                                unset($item[$name . '_old']);
                            }

                            //Save the new image
                            $item['image'] = self::image($files[$index][$name]);
                        }
                    }
                }

                $item['updated_at'] = date('Y-m-d H:i:s');

                //Update the blocks info
                DB::table($table)->where('id', $item['id'])->update($item);
                if (!empty($output)) {
                    $output .= ','.$item['id'];
                }
                else {
                    $output = $item['id'];
                }
            }

            else {
                $item['created_at'] = date('Y-m-d H:i:s');
                if (in_array('image', $block->getTypes())) {
                    $item['image'] = !empty($item['id']) && !empty($files[$item['id']]) ? Save::image($files[$item['id']]['image']) : 1;
                }
                if (empty($output)) $output = DB::table($table)->insertGetId($item);
                else $output .= ',' . DB::table($table)->insertGetId($item);
            }
        }

        foreach ($oldBlocks as $oldBlock) {
            DB::table($table)->where('id', $oldBlock)->delete();
        }

        return isset($output) ? $output : '';
    }

    /**
     * Save a Image
     *
     * @param $file
     * @param mixed $imageId
     * @return int
     */
    static function image($file, $imageId = null)
    {
        if(!empty($imageId)) {
            Delete::image($imageId);
        }

        $image = Storage::putFile('public/images', $file);

        $min = \Tinify\fromFile(storage_path('app/' . $image));
        $min->toFile(storage_path('app/' . $image));

        $image = explode('/', $image);

        $image = $image[2];

        $output = DB::table('images')->insertGetId(['source' => $image]);
        return $output;
    }
}