<?php

namespace Freshfield\Core\Processing;

use Freshfield\Core\Base\Block\BaseBlock;
use Freshfield\Core\Base\Form\BaseForm;
use Freshfield\Core\Base\Lists\BaseList;
use Illuminate\Support\Facades\DB;

class Retrieve
{
    /**
     * Retrieve a Form by type
     *
     * @param string $type
     * @return BaseForm
     */
    static function form(string $type)
    {
        if (!isset(config('freshfield')['forms'][$type])) {
            abort(404);
        }

        $form = config('freshfield')['forms'][$type];

        return new $form;
    }

    /**
     * Retrieve a List by type
     *
     * @param string $type
     * @return BaseList
     */
    static function list(string $type)
    {

        if (!isset(config('freshfield')['lists'][$type])) {
            abort(404);
        }

        $list = config('freshfield')['lists'][$type];

        return new $list;
    }

    /**
     * Retrieve a Block by type
     *
     * @param string $type
     * @return BaseBlock
     */
    static function block(string $type) {

        if (empty(config('freshfield')['blocks'][$type])) {
            abort(404);
        }

        $block = config('freshfield')['blocks'][$type];

        return new $block;
    }

    /**
     * Retrieve all the necessary data for a form
     *
     * @param BaseForm $form
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    static function retrieveFormData(BaseForm $form, $id)
    {
        $orderBy = $form->getSort();
        $items = self::filterItems($form->getItems());
        $query = DB::table($form->getTable())->select($items)->where($form->getIndex(), $id);

        foreach($orderBy as $item) {
            $order = explode(' ', $item);
            $query->orderBy($order[0], $order[1]);
        }

        $results = $query->first();

        if (!empty($form->getForeign())) {
            $results = self::__getForeign($form, $results);
        }
        if (!empty($form->getTypes() && in_array('blocks', $form->getTypes()))) {
            $results = self::__getAllBlocks($form, $results);
        }
        if (!empty($form->getTypes()) && in_array('json', $form->getTypes())) {
            $results = self::__getJson($form, $results);
        }

        return $results;
    }

    /**
     * Filter out the JSON items and the Block array's
     *
     * @param string[] $items
     * @return string[]
     */
    static function filterItems($items) {
        $output = [];
        foreach($items as $key => $item) {
            if (is_array($item)) {
                $output[] = $key;
            }
            else if (str_contains($item, '.')) {
                $name = explode('.', $item)[0];
                if (!in_array($name, $output)) {
                    $output[] = $name;
                }
            }
            else {
                $output[] = $item;
            }
        }

        return $output;
    }

    /**
     * Get's the form and the results of the primary query
     * Returns the primary query but with all the foreign key data
     *
     * @param BaseForm $form
     * @param $mainResults
     * @return mixed
     */
    static function __getForeign(BaseForm $form, $mainResults) {
        $output = $mainResults;

        foreach ($form->getForeign() as $key => $value) {
            $foreign = explode('.', $value);
            /**
             * @var $foreignForm BaseForm
             */
            $foreignForm = Retrieve::form($foreign[0]);

            if (($form->getTypes())[$key] === 'select') {
                $data = DB::table($foreignForm->getTable())->select('*')->where($foreign[1], $output->{$key})->whereNull('deleted_at')->get();
                $total = DB::table($foreignForm->getTable())->select('*')->whereNull('deleted_at')->get();
                if (!empty($data[0]->{$foreign[2]})) {
                    $output->{$key} = ['value' => ['id' => $data[0]->id, 'value' => $data[0]->{$foreign[2]}], 'total' => $total];
                } else {
                    $output->{$key} = ['value' => null, 'total' => $total];
                }
            } else if ($form->getTypes()[$key] === 'blocks') {
                $data = $output->{$key};

                $blocks = strpos($data, ',') !== false ? explode(',', $data) : [$data];
                $output->{$key} = ['blocks' => []];
                foreach ($blocks as $blockId) {
                    $output->{$key}['blocks'][] = DB::table($key)->select('*')->where('id', $blockId)->whereNull('deleted_at')->first();
                    if(!empty($output->{$key}['blocks'][count($output->{$key}['blocks']) - 1]->image)) {
                        $output->{$key}['blocks'][count($output->{$key}['blocks']) - 1]->image = [
                            'old' => $output->{$key}['blocks'][count($output->{$key}['blocks']) - 1]->image,
                            'source' => DB::table('images')->
                            select('source')->
                            where('id', $output->{$key}['blocks'][count($output->{$key}['blocks']) - 1]->image)->
                            whereNull('deleted_at')->first(),
                        ];
                    }
                }
            } else {
                $data = DB::table($foreignForm->getTable())->select($foreign[2])->where($foreign[1], $output->{$key})->whereNull('deleted_at')->first();
                if (!empty($data->{$foreign[2]})) {
                    $output->{$key} = $data->{$foreign[2]};
                } else {
                    $output->{$key} = null;
                }
            }
        }

        return $output;
    }

    /**
     * Get's the form and the results of the primary query
     * Returns the primary query but each item with type json has been converted from plain text to json
     *
     * @param BaseForm $form
     * @param $mainResults
     * @return mixed
     */
    static function __getJson(BaseForm $form, $mainResults) {
        $types = $form->getTypes();

        foreach($types as $item => $type) {
            if ($type === 'json') $mainResults->{$item} = json_decode($mainResults->{$item});
        }

        return $mainResults;
    }

    /**
     * Get the linked Blocks data
     *
     * @param BaseForm $form
     * @param $mainResults
     * @return mixed
     */
    static function __getAllBlocks(BaseForm $form, $mainResults) {
        $types = $form->getTypes();
        $items = $form->getItems();

        foreach($types as $name => $type) {
            if ($type === 'blocks') {
                if (is_array($items[$name])) {
                    $block = self::block($name);
                    $blockIds = explode(',', $mainResults->{$name});
                    $output = [];
                    if (!empty($blockIds[0])) {
                        foreach($blockIds as $blockId) {
                            $output[] = self::__getSingleBlock($block, $blockId);
                        }
                    }
                    $mainResults->{$name . '_old'} = $mainResults->{$name};
                    $mainResults->{$name} = $output;

                }
            }
        }

        return $mainResults;
    }

    /**
     * @param BaseBlock $block
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|null|object
     */
    static function __getSingleBlock(BaseBlock $block, $id) {
        return DB::table($block->getTable())->select($block->getItems())->where('id', $id)->whereNull('deleted_at')->orderBy('prio', 'ASC')->first();
    }

    /**
     * Retrieve the necessary data for building a List
     *
     * @param string $table
     * @param array $sort
     * @param array|null $filter
     * @return \Illuminate\Support\Collection
     */
    static function retrieveListData(string $table, array $sort, ?array $filter) {
        if(!empty($filter)) {
            $query = DB::table($table)->select('*')->where($filter['index'], $filter['value'])->whereNull('deleted_at')->orderBy('created_at', 'DESC');
        } else {
            $query = DB::table($table)->select('*')->whereNull('deleted_at')->orderBy('created_at', 'DESC');
        }
        foreach($sort as $value) {
            $item = explode(' ', $value);
            $query->orderBy($item[0], $item[1]);
        }
        return $query->get();
    }

    /**
     * Retrieve the necessary data for building a Trash List
     *
     * @param string $table
     * @param array $sort
     * @return \Illuminate\Support\Collection
     */
    static function retrieveTrashListData(string $table, array $sort) {
        $query = DB::table($table)->select('*')->whereNotNull('deleted_at')->orderBy('deleted_at', 'DESC');

        foreach($sort as $value) {
            $item = explode(' ', $value);
            $query->orderBy($item[0], $item[1]);
        }

        return $query->get();
    }

    /**
     * Get the Foreign Key Data
     *
     * @param BaseForm $form
     * @return array
     */
    static function foreignKey(BaseForm $form)
    {
        $output = [];
        $types = $form->getTypes();
        foreach ($form->getForeign() as $key => $foreign) {
            if ($types[$key] === 'select') {
                $info = explode('.', $foreign);
                $foreignForm = Retrieve::form($info[0]);
                $data = DB::table($foreignForm->getTable())->select('*')->whereNull('deleted_at')->get();
                foreach ($data as $item) {
                    $output[$key][] = [ 'id' => $item->id, 'value' => $item->{$info[2]} ];
                }
            }
        }

        return $output;
    }

    /**
     * Get all the Info of a specific Block Type
     *
     * @param $name
     * @return object
     */
    static function getBlockInfo($name) {
        $block = self::block($name);

        return (object)[
            'types' => $block->getTypes(),
            'items' => $block->getItems(),
            'invisible' => $block->getInvisible(),
            'required' => $block->getRequired(),
            'labels' => $block->getLabels(),
            'name' => $block->getName(),
            'blockTypes' => $block->getBlocks(),
            'icons' => $block->getIcons(),
        ];
    }

    /**
     * Retrieving all data there is of a specific entry
     *
     * @param BaseForm $form
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Query\Builder|mixed|null|object
     */
    static function retrieveAllData($form, $id) {
        $orderBy = $form->getSort();
        $items = self::filterItems($form->getItems());
        $query = DB::table($form->getTable())->select($items)->where($form->getIndex(), $id);

        foreach($orderBy as $item) {
            $order = explode(' ', $item);
            $query->orderBy($order[0], $order[1]);
        }

        $results = $query->first();

        if (!empty($form->getForeign())) {
            $results = self::__getForeign($form, $results);
        }
        if (!empty($form->getTypes() && in_array('blocks', $form->getTypes()))) {
            $results = self::__getAllBlocks($form, $results);
        }
        if (!empty($form->getTypes()) && in_array('json', $form->getTypes())) {
            $results = self::__getJson($form, $results);
        }

        return $results;
    }
}