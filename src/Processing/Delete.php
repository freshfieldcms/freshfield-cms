<?php

namespace Freshfield\Core\Processing;


use Freshfield\Core\Base\Form\BaseForm;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Delete
{
    /**
     * Delete the item
     *
     * @param string $type
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    static function delete(string $type, $id)
    {
        $form = Retrieve::form($type);

        if(in_array('image', $form->getTypes())) {
            $values = Retrieve::retrieve($form, $id);
            $index = array_Search('image', $form->getTypes());
            if ($values->{$index} != 1) {
                Delete::image($values->{$index});
            }
        } else if (in_array('blocks', $form->getTypes())) {
            $values = Retrieve::retrieve($form, $id);
            $index = array_Search('blocks', $form->getTypes());
            foreach($values->{$index}['blocks'] as $block) {
                if ($block->type === 'image' && $block->image['old'] != 1) {
                    Delete::image(($block->image)['old']);
                }
            }
        }

        if(DB::table($form->getTable())->where('id', $id)->update(['deleted_at' => date('Y-m-d H:i:s')])) {
            return redirect()->route('list', ['type' => $type]);
        }
        return redirect()->route('error');
    }

    /**
     * Delete the image
     *
     * @param $imageId
     */
    static function image($imageId) {
        if ($imageId == 1 || empty($imageId)) return;
        $imageSource = DB::table('images')->select('source');
        if (gettype($imageId) !== 'integer') $imageSource->where('source', $imageId);
        else $imageSource->where('id', $imageId);

        $imageSource = $imageSource->first();
        if(empty($imageSource) || !Storage::exists('public/images/'.$imageSource->source)) return;
        Storage::move('public/images/'.$imageSource->source, 'public/deleted/'.$imageSource->source);
        if (Storage::exists('public/images/'.$imageSource->source) && Storage::exists('public/deleted/'.$imageSource->source)) {
            Storage::delete('public/images/'.$imageSource->source);
        }
        $time = date('Y-m-d H:i:s');
        $query = DB::table('images');
        if (gettype($imageId) !== 'integer') $query->where('source', $imageId);
        else $query->where('id', $imageId);
        $query->update(['updated_at' => $time, 'deleted_at' => $time]);
    }
}