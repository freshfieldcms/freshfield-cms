@extends('cms::layouts.app')

@section('content')

    <div class="base">

        @if($type === 'success')
            <h1 class="success">
                Everything completed successfully! Good job system :)
            </h1>
        @else
            <h1 class="error">
                Whoops... Something went terribly wrong! Please report this to the developer! :C
            </h1>
        @endif

    </div>

@endsection