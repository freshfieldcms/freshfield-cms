@extends('cms::layouts.app')

@include('cms::popup')

@section('content')

    <div class="base">

        @if(!empty($title))
            <div class="base__header">
                <h1 class="base__title base-title">
                    {{ $title }}
                </h1>
                <div class="base__buttons">
                    <a href="{{ route('create', ['type' => $type]) }}" class="base__button base__button--success">
                        Create
                    </a>
                    @if(empty($trash))
                        <a href="{{ route('trash', ['type' => $type]) }}" class="base__button base__button--edit">
                            Trash
                        </a>
                    @endif
                </div>
            </div>
        @endif

        <table class="list">

            <thead>
                <tr class="list__head list-head">
                    @foreach($visible as $item)
                        <th class="list-head__title @if(in_array($item, $mobile)) list-head__title--mobile @endif">{{$labels[$item]}}</th>
                    @endforeach
                    <th>
                        <!-- @TODO MAKE FILTERS FUNCTIONAL -->
                        {{--<button class="base__button base__button--dark list-head__button" type="button" onclick="showFilters()">Filters</button>--}}
                    </th>
                </tr>
                <tr>
                    <!-- @TODO MAKE FILTERS FUNCTIONAL -->
                    <!--<th class="list__filters list-filters" colspan="{{count($visible) + 1}}">
                        <div class="list-filters__container filters" id="filters">
                            <div class="filters__item filters-item">
                                <div class="filters-item__label">Filter 1</div>
                                <div class="filters-item__value">
                                    <div class="form-row__select form-select">
                                        <div class="form-select__current form-item__value" id="select-current" data-input="select-value" onclick="toggleSelect('select-items')">
                                            Select an option
                                            <i class="fas fa-caret-down"></i>
                                        </div>
                                        <div class="form-select__group form-select-group" id="select-items">
                                            <div class="form-select-group__item" data-parent="select-current" data-value="1" onclick="changeSelect(this)">
                                                Option 1
                                            </div>
                                            <div class="form-select-group__item" data-parent="select-current" data-value="2" onclick="changeSelect(this)">
                                                Option 2
                                            </div>
                                            <div class="form-select-group__item" data-parent="select-current" data-value="3" onclick="changeSelect(this)">
                                                Option 3
                                            </div>
                                            <div class="form-select-group__item" data-parent="select-current" data-value="4" onclick="changeSelect(this)">
                                                Option 4
                                            </div>
                                            <div class="form-select-group__item" data-parent="select-current" data-value="5" onclick="changeSelect(this)">
                                                Option 5
                                            </div>
                                            <div class="form-select-group__item" data-parent="select-current" data-value="6" onclick="changeSelect(this)">
                                                Option 6
                                            </div>
                                            <div class="form-select-group__item" data-parent="select-current" data-value="7" onclick="changeSelect(this)">
                                                Option 7
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </th> -->
                </tr>
            </thead>
            <tbody>
                @foreach($items as $row => $item)
                    @if(!empty($item))
                        <tr class="list__row list-row" onclick="onClickAnimation('list-menu-{{$row}}')">
                            @foreach($visible as $key => $value)
                                <td class="list-row__item @if(isset($mobile[$key])) list-row__item--mobile @endif">{{ ($item->{$value}) }}</td>
                            @endforeach
                            <td class="list-row__item list-row__item--mobile list-row__item--buttons">
                                <div class="list-row__group">
                                    @if(!empty($trash))
                                        <button type="button" onclick="togglePopup('confirm', () => location.href = '{{ route('restore', ['type' => $type, 'id' => $item->id]) }}')" class="base__button base__button--success list-row__item--success">
                                            <i class="fas fa-undo"></i>
                                        </button>
                                    @else
                                        <button type="button" onclick=" location.href= '{{ route('edit', ['type' => $type, 'id' => $item->id]) }}'" class="base__button base__button--edit list-row__item--edit">
                                            <i class="fas fa-pencil-alt"></i>
                                        </button>
                                        <button type="button" onclick="togglePopup('confirm', () => location.href = '{{ route('delete', ['type' => $type, 'id' => $item->id]) }}')" class="base__button base__button--danger list-row__item--delete">
                                            <i class="fas fa-trash"></i>
                                        </button>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>

        </table>

    </div>

@endsection