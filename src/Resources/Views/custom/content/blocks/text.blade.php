<div class="blocks-list__item blocks-item" id="block-{{$id}}-{{$data->id}}">
    <div class="blocks-item__header" onclick="editBlock('{{$id}}-{{$data->id}}')" id="header-{{$id}}-{{$data->id}}">
        <div style="display: flex">
            <div class="blocks-item__header--icon">
                <i class="fas fa-font"></i>
            </div>
            <div class="blocks-item__header--title">
                Text block
            </div>
        </div>
        @if(!isset($show))
            <button type="button" onclick="removeBlock('block-{{$id}}-{{$data->id}}')" class="blocks-item__header--delete">
                <i class="fas fa-trash-alt"></i></button>
        @endif
    </div>

    <div class="blocks-item__form blocks-form" id="form-{{$id}}-{{$data->id}}">
        <input type="hidden" name="{{$id}}[{{$data->id}}][type]" value="text">
        <div class="blocks-form__item blocks-form-item">
            <label class="blocks-form-item__label" for="{{ $data->id}}-title">Title</label>
            <input class="blocks-form-item__input form-item__value" type="text" name="{{$id}}[{{$data->id}}][title]"
                   id="{{$data->id}}-title" value="{{ !empty($data) && !empty($data->title) ? $data->title : ''}}"
                   placeholder="Title" @if(isset($show)) disabled @endif>
        </div>
        <div class="blocks-form__item blocks-form-item">
            <label class="blocks-form-item__label" for="{{$data->id}}-text">Text</label>
            <textarea class="blocks-form-item__input form-item__value tinymce" name="{{$id}}[{{$data->id}}][text]"
                      placeholder="Text" @if(isset($show)) disabled @endif
                      id="{{$data->id}}-text"> {{ !empty($data) && !empty($data->text) ? $data->text : ''}}</textarea>
        </div>
    </div>
</div>