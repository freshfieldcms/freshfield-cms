<div class="blocks-list__item blocks-item" id="block-{{$id}}-{{$data->id}}">
    <div class="blocks-item__header" onclick="editBlock('{{$id}}-{{$data->id}}')" id="header-{{$id}}-{{$data->id}}">
        <div style="display: flex">
            <div class="blocks-item__header--icon">
                <i class="fas fa-image"></i>
            </div>
            <div class="blocks-item__header--title">
                Image block
            </div>
        </div>
        @if(!isset($show))
            <button type="button" onclick="removeBlock('block-{{$id}}-{{$data->id}}')" class="blocks-item__header--delete"><i class="fas fa-trash-alt"></i></button>
        @endif
    </div>

    <div class="blocks-item__form blocks-form" id="form-{{$id}}-{{$data->id}}">
        <input type="hidden" name="{{$id}}[{{$data->id}}][type]" value="image">
        <div class="blocks-form__item blocks-form-item">
            <label class="blocks-form-item__label" for="{{$data->id}}-title">Image</label>
            @if(!isset($show))
                <input type="file" name="{{$id}}[{{$data->id}}][image]" id="{{$data->id}}-image">
            @endif
            @if(!empty($data) && !empty($data->image))
                <img src="{{ '/storage/public/images/'.$data->image['source']->source }}" class="blocks-form-item__image" alt="image">
                <input type="hidden" name="{{$id}}[{{$data->id}}][image_old]" value="{{$data->image['old']}}">
            @endif
        </div>
        <div class="blocks-form__item blocks-form-item">
            <label class="blocks-form-item__label" for="{{$data->id}}-text">Text</label>
            @if(!isset($show))
                <input class="blocks-form-item__input form-item__value" type="text" name="{{$id}}[{{$data->id}}][text]" id="{{$data->id}}-text" value="{{ !empty($data) && !empty($data->text) ? $data->text : ''}}" placeholder="Text">
            @endif
        </div>
    </div>
</div>