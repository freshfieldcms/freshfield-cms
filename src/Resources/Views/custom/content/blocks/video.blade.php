<div class="blocks-list__item blocks-item" id="block-{{$id}}-{{$data->id}}">
    <div class="blocks-item__header" onclick="editBlock('{{$id}}-{{$data->id}}')" id="header-{{$id}}-{{$data->id}}">
        <div style="display: flex">
            <div class="blocks-item__header--icon">
                <i class="fas fa-video"></i>
            </div>
            <div class="blocks-item__header--title">
                Video block
            </div>
        </div>
        <button type="button" onclick="removeBlock('block-{{$id}}-{{$data->id}}')" class="blocks-item__header--delete"><i class="fas fa-trash-alt"></i></button>
    </div>

    <div class="blocks-item__form blocks-form" id="form-{{$id}}-{{$data->id}}">
        <input type="hidden" name="{{$id}}[{{$data->id}}][type]" value="video">
        <div class="blocks-form__item blocks-form-item">
            <label class="blocks-form-item__label" for="{{ $data->id}}-title">Title</label>
            <input class="blocks-form-item__input form-item__value" type="text" name="{{$id}}[{{$data->id}}][title]" id="{{$data->id}}-title" value="{{ !empty($data) && !empty($data->title) ? $data->title : ''}}" placeholder="Title" @if(isset($show)) disabled @endif>
        </div>
        <div class="blocks-form__item blocks-form-item">
            <label class="blocks-form-item__label" for="{{$data->id}}-video-url">Video URL</label>
            <input class="blocks-form-item__input form-item__value" type="text" name="{{$id}}[{{$data->id}}][video_url]" id="{{$data->id}}-video-url" value="{{ !empty($data) && !empty($data->video_url) ? $data->video_url : ''}}" placeholder="Video URL" @if(isset($show)) disabled @endif>
        </div>
    </div>
</div>