<script src="{{ asset('vendor/freshfield/js/axios.min.js') }}"></script>
<script src="{{ asset('vendor/freshfield/js/contentBlocks.js') }}"></script>

<div class="blocks">

    <div class="blocks__head blocks-head">
        Blocks
        @if(!isset($show))
            <div class="blocks__add blocks-add">
                <button type="button" class="blocks-add__button" id="blocks-button" onclick="toggleChooseBlock()"><i class="fas fa-plus"></i></button>
                <div class="blocks-add__container blocks-add-container" id="blocks-container">
                    <button type="button" class="blocks-add-container__button" onclick="addBlock('title', '{{$id}}')"><i class="fas fa-text-height"></i>Title</button>
                    <button type="button" class="blocks-add-container__button" onclick="addBlock('text', '{{$id}}')"><i class="fas fa-font"></i>Text</button>
                    <button type="button" class="blocks-add-container__button" onclick="addBlock('image', '{{$id}}')"><i class="fas fa-image"></i>Image</button>
                    <button type="button" class="blocks-add-container__button" onclick="addBlock('video', '{{$id}}')"><i class="fas fa-video"></i>Video</button>
                </div>
            </div>
        @endif
    </div>
    <div class="blocks__list blocks-list sortable" id="blocks-{{$id}}">

        @if(!empty($blocks))
            @foreach($blocks as $block)
                @if(empty($block)) @continue @endif
                    <div id="block-{{$id}}-{{$block->id}}" draggable="true" ondragstart="drag(event)" ondrop="drop(event)">
                        @if ($block->type === 'text')
                            @include('components.blocks.text', ['data' => $block, 'show' => $show])
                        @elseif($block->type === 'image')
                            @include('components.blocks.image', ['data' => $block, 'show' => $show])
                        @elseif($block->type === 'video')
                            @include('components.blocks.video', ['data' => $block, 'show' => $show])
                        @elseif($block->type === 'title')
                            @include('components.blocks.title', ['data' => $block, 'show' => $show])
                        @endif
                    </div>
                    <script>countBlocks = {{$block->id}};</script>
            @endforeach
        @endif

    </div>

</div>