<nav class="navigation">
    <div class="navigation__mobile navigation-mobile">
        <img src="{{ asset('/img/logo-white.png') }}" class="navigation-mobile__logo" />
        <button type="button" onclick="toggleNavigation()" class="navigation-mobile__button">
            {!! file_get_contents(asset('/svg/hamburger.svg')) !!}
        </button>
    </div>
    <div class="navigation__main" id="navigation">
        <div class="navigation__item navigation__item--first navigation-item">
            <div class="navigation__link navigation__link--no-hover navigation-link">
                <span class="navigation-link__text navigation-link__text--full">Welcome {{ auth()->user()->name }}!</span>
            </div>
        </div>
        <div class="navigation__group">
            @if(isset($routes))
                @foreach($routes as $route)

                    @if(count($route['childs']) >= 1)

                        <div class="navigation__item navigation-item @if($_SERVER['REQUEST_URI'] === $route['main']->url) navigation__item--active @endif">
                            <div class="navigation__link navigation-link"
                                 onclick="toggleNavigationDropdown('dropdown{{ $route['main']->id }}')">
                                <span class="navigation-link__text">{{$route['main']->name}}</span>
                                <i class="fas fa-caret-down navigation-link__icon"></i>
                            </div>
                            <div class="navigation__dropdown navigation-dropdown" id="dropdown{{$route['main']->id}}">
                                <div class="navigation-dropdown__item navigation-dropdown-item"
                                     onclick="location.href='{{$route['main']->url}}'">
                                    {{ $route['main']->name }}
                                </div>
                                @foreach($route['childs'] as $child)

                                    <div class="navigation-dropdown__item navigation-dropdown-item"
                                         onclick="location.href='{{$child->url}}'">
                                        {{ $child->name }}
                                    </div>

                                @endforeach
                            </div>
                        </div>

                    @else

                        <div class="navigation__item navigation-item @if($_SERVER['REQUEST_URI'] === $route['main']->url) navigation__item--active @endif"
                             onclick="location.href='{{$route['main']->url}}'">
                            <div class="navigation__link navigation-link"
                                 onclick="location.href='{{$route['main']->url ? $route['main']->url : ''}}'">
                                <span class="navigation-link__text">{{$route['main']->name}}</span>
                            </div>
                        </div>

                    @endif

                    <hr class="navigation__divider" />

                @endforeach
            @endif
        </div>
        <div class="navigation__bottom navigation-bottom">
            <div class="navigation-bottom__item">
                <a class="navigation-bottom__link" href="{{route('profile')}}">
                    <i class="fas fa-user-edit"></i>
                </a>
            </div>
            <div class="navigation-bottom__item">
                <a class="navigation-bottom__link" href="{{route('settings')}}" onclick="document.getElementById('logout-form').submit();">
                    <i class="fas fa-cog"></i>
                </a>
            </div>
            <div class="navigation-bottom__item">
                <a class="navigation-bottom__link" href="#" onclick="document.getElementById('logout-form').submit();">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            </div>
        </div>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</nav>