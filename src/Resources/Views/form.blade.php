@extends('cms::layouts.app')

@section('content')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=3neiqoc9okdd0x41xgwjt5t6sqjyyj3uzolonk66e74zy3b1"></script>

    <div class="base">

        @if(!empty($title))
            <div class="base__header">
                <h1 class="base__title">
                    {{ $title }}
                </h1>
            </div>
        @endif

        @if(!empty($errors->default->all()))
            <div class="form__errors form-errors">
                @foreach($items as $item)
                    @if(!empty($errors->default->get($item)))
                        <div class="form-errors__row">
                            <div class="form-errors__title">{{ !empty($labels[$item]) ? $labels[$item] : $item }}</div>
                            @foreach($errors->default->get($item) as $error)
                                <div class="form-errors__item">
                                    {{ $error }}
                                </div>
                            @endforeach
                        </div>
                    @endif
                @endforeach
            </div>
        @endif

        <form action="{{ route('save', ['type' => $type])  }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form">

                @foreach($items as $name => $item)
                    <div class="form__row form-row @if(gettype($item) !== 'array' && !empty($errors->default->get($item))) form__row--error @endif">
                        @if(gettype($item) === 'array')

                            @if(isset($types[$name]) && $types[$name] === 'json')

                                @include('cms::parts.subject', ['items' => $item, 'title' => $name])

                            @elseif(!empty($types[$name]) && $types[$name] === 'blocks')

                                <div class="form-row__item form-row__item--label form-item">
                                    <label class="form-item__label">{{ !empty($labels[$name]) ? $labels[$name] : $name }}</label>
                                </div>
                                <div class="form-row__item form-row__item--input form-item">
                                    @if(isset($values))
                                        @include('cms::forms.hidden', ['name' => $name.'[old_blocks]', 'id' => $name.'[old_blocks]', 'value' => $values->{$name.'_old'}])
                                    @endif
                                    @include('cms::components.block', ['block' => $blocks[$name]])
                                </div>
                            @endif

                        @elseif((!empty($types[$item]) && $types[$item] === 'hidden') || in_array($item, $invisible))

                            <input type="hidden" name="{{$item}}" id="{{$item}}"
                                   value="{{ !empty($values) && !empty($values->{$item}) ? $values->{$item} : ''}}">

                        @elseif(!empty($types[$item]) && $types[$item] === 'disabled')

                            <div class="form-row__item form-row__item--label form-item">
                                <label class="form-item__label">{{ !empty($labels[$item]) ? $labels[$item] : $item }}</label>
                            </div>
                            <div class="form-row__item form-row__item--input form-item">
                                <input class="form-item__value form-item__value--disabled" type="{{isset($types[$item]) ? $types[$item] : 'text'}}"
                                       value="{{ !empty($values) && !empty($values->{$item}) ? $values->{$item} : ''}}"
                                       disabled>
                                <input class="form-item__value" type="{{isset($types[$item]) ? $types[$item] : 'text'}}"
                                       name="{{$item}}" id="{{$item}}"
                                       value="{{ !empty($values) && !empty($values->{$item}) ? $values->{$item} : ''}}"
                                       hidden>
                            </div>

                        @elseif(!empty($types[$item]) && $types[$item] === 'image')

                            <div class="form-row__item form-row__item--label form-item">
                                <label class="form-item__label" for="{{$item}}">{{ !empty($labels[$item]) ? $labels[$item] : $item }}</label>
                            </div>
                            <div class="form-row__item form-row__item--multiple form-row__item--input form-item">
                                <input type="file" name="{{$item}}" id="{{$item}}" onchange="replaceOldImage(this)" value="@if(!empty(old($item))){{old($item)}}@endif" hidden>
                                <button type="button" class="base__button base__button--success" onclick="triggerImageUpload('{{$item}}')">Upload image</button>
                                @if(!empty($values) && !empty($values->{$item}))
                                    <img id="old_{{$item}}" src="{{ '/storage/images/'.$values->{$item} }}" alt="Image"
                                         class="form-item__image">
                                @endif
                            </div>


                        @elseif(!empty($types[$item]) && $types[$item] === 'password')
                            <div class="form-row__item form-row__item--label form-item">
                                <label class="form-item__label"
                                       for="{{$item}}">{{ !empty($labels[$item]) ? $labels[$item] : $item }}</label>
                            </div>
                            <div class="form-row__item form-row__item--input form-item">
                                <input class="form-item__value" type="password" name="{{$item}}" id="{{$item}}" value=""
                                       placeholder="{{ !empty($labels[$item]) ? $labels[$item] : $item }}"
                                       @if(empty($values) || empty($values->{$item})) required @endif>
                            </div>
                    </div>
                     <hr class="form__divider" />
                    <div class="form__row form-row">
                        <div class="form-row__item form-row__item--label form-item">
                            <label class="form-item__label"
                                   for="{{$item}}_confirmation">Confirm {{ !empty($labels[$item]) ? $labels[$item] : $item }}</label>
                        </div>
                        <div class="form-row__item form-row__item--input form-item">
                            <input class="form-item__value" type="password" name="{{$item}}_confirmation"
                                   id="{{$item}}_confirmation" value=""
                                   placeholder="Confirm {{ !empty($labels[$item]) ? $labels[$item] : $item }}"
                                   @if(empty($values) || empty($values->{$item})) required @endif>
                        </div>

                        @elseif(!empty($types[$item]) && $types[$item] === 'select')
                            <div class="form-row__item form-row__item--label form-item">
                                <label class="form-item__label">
                                    {{ !empty($labels[$item]) ? $labels[$item] : $item }}
                                </label>
                            </div>
                            <div class="form-row__item form-row__item--input form-item">

                                <div class="form-row__select form-select">
                                    <input type="hidden" name="{{$item}}" id="{{$item}}-value" value="@if(!empty($values->{$item}['value'])) {{$values->{$item}['value']['id']}} @else  @endif">
                                    <div class="form-select__current form-item__value" id="{{$item}}-current" data-input="{{$item}}-value" onclick="toggleSelect('{{$item}}-items')">
                                        @if(!empty($values->{$item}['value']))
                                            {{ $values->{$item}['value']['value'] }}
                                        @else
                                            Select an option
                                        @endif
                                        <i class="fas fa-caret-down"></i>
                                    </div>
                                    <div class="form-select__group form-select-group" id="{{$item}}-items">
                                        @if (!empty($values->{$item}['value']))
                                            <div class="form-select-group__item" data-parent="{{$item}}-current" data-value="{{ $values->{$item}['value']['id'] }}" onclick="changeSelect(this)">
                                                {{ $values->{$item}['value']['value'] }}
                                            </div>
                                        @endif
                                        @foreach($selectData[$item] as $option)
                                            @if(empty($values->{$item}['value']) || $option['id'] !== $values->{$item}['value']['id'])
                                                <div class="form-select-group__item" data-parent="{{$item}}-current" data-value="{{ !empty($option) ? $option['id'] : ''}}" onclick="changeSelect(this)">
                                                    {{ $option['value'] }}
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                        @elseif(!empty($types[$item]) && $types[$item] === 'boolean')
                            <div class="form-row__item form-row__item--label form-item">
                                <label class="form-item__label">
                                    {{ !empty($labels[$item]) ? $labels[$item] : $item }}
                                </label>
                            </div>
                            <div class="form-row__item form-row__item--input form-item">
                                <label class="form-item-checkbox" for="{{$item}}">
                                    <input
                                            type="checkbox"
                                            class="form-item-checkbox__input"
                                            name="{{$item}}"
                                            id="{{$item}}"
                                            @if((!empty($values) && !empty($values->{$item})) || !empty(old($item))) checked @endif
                                    />
                                    <span class="form-item-checkbox__mask"></span>
                                </label>
                            </div>

                        @elseif(!empty($types[$item]) && $types[$item] === 'tinymce')
                            <div class="form-row__item form-row__item--label form-item">
                                <label class="form-item__label" for="{{$item}}">
                                    {{ !empty($labels[$item]) ? $labels[$item] : $item }} @if(in_array($item, $required)) * @endif
                                </label>
                            </div>
                            <div class="form-row__item form-row__item--input form-item">
                                <textarea class="tinymce"
                                          name="{{$item}}" id="{{$item}}"
                                          placeholder="{{ !empty($labels[$item]) ? $labels[$item] : $item }}"
                                          @if(in_array($item, $required)) required @endif
                                >
                                    {{ !empty(old($item)) ? old($item) : (!empty($values) && !empty($values->{$item}) ? $values->{$item} : '')}}
                                </textarea>
                            </div>

                        @else
                            <div class="form-row__item form-row__item--label form-item">
                                <label class="form-item__label" for="{{$item}}">
                                    {{ !empty($labels[$item]) ? $labels[$item] : $item }} @if(in_array($item, $required))* @endif
                                </label>
                            </div>
                            <div class="form-row__item form-row__item--input form-item">
                                <input class="form-item__value"
                                       type="{{isset($types[$item]) ? $types[$item] : 'text'}}"
                                       name="{{$item}}" id="{{$item}}"
                                       value="{{ !empty(old($item)) ? old($item) : (!empty($values) && !empty($values->{$item}) ? $values->{$item} : (!empty($types[$item]) && $types[$item] === 'number' ? 0 : ''))}}"
                                       placeholder="{{ !empty($labels[$item]) ? $labels[$item] : $item }}"
                                       @if(in_array($item, $required)) required @endif>
                            </div>

                        @endif
                    </div>

                    <hr class="form__divider" />

                @endforeach

                <div class="form__buttons">
                    <button type="submit" class="base__button base__button--success">Save</button>
                    <button type="button" onclick="location.href = '{{ $type !== 'profile' ? $back_url : route('list', ['type' => 'users'])}}'" class="base__button base__button--danger">Cancel</button>
                </div>
            </div>
        </form>
    </div>

    <script>
        tinymce.init({
            selector: ".tinymce",
            plugins: "code hr wordcount",
            toolbar: "code hr wordcount",
            force_br_newlines: true,
            force_p_newlines: false,
            forced_root_block: ''
        });
    </script>

@endsection