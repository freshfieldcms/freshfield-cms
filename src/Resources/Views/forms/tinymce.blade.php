<textarea class="tinymce"
        name="{{$name}}"
        id="{{$id}}"
        placeholder="{{ $placeholder }}"
        @if($required === true)required @endif
>
    @if(!empty($value)){{$value}}@endif
</textarea>