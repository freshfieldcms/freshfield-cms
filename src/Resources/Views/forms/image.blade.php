<input type="file" name="{{$name}}" id="{{$id}}" onchange="replaceOldImage(this)" hidden>
<button type="button" class="base__button base__button--success" id="{{$id}}-BUTTON" onclick="triggerImageUpload('{{$id}}')">Upload image</button>
@if(!empty($value))
    <img id="old_{{$name}}" src="{{ '/storage/images/'.$value }}" alt="Image" class="form-item__image">
@endif