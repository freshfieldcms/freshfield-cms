<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Freshfield</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="{{ asset('/css/auth.css') }}" rel="stylesheet"/>
        <script src="{{ asset('/js/auth.js') }}"></script>
    </head>
    <body>
        <div class="auth">
            <div class="auth__container">
                <div class="content">
                    <img src="{{ asset('/img/logo.png') }}" class="content__logo" />

                    <form class="content__form form" method="post" action="{{ route('login-post') }}">
                        @csrf

                        @if(!empty(session('login-error')))
                            <div class="form__item form-item">
                                <div class="form-item__error">
                                    {{ session('login-error') }}
                                </div>
                            </div>

                            @php(session()->forget('login-error'))
                        @endif

                        @if(!empty(session('message')))
                            <div class="form__item form-item">
                                <div class="form-item__error">
                                    {{ session('message') }}
                                </div>
                            </div>

                            @php(session()->forget('message'))
                        @endif

                        <div class="form__item form-item">
                            <label class="form-item__label" for="freshfield_name">Username</label>
                            <div class="form-item__input" id="freshfield_name_parent">

                                <span class="form-item__icon">{!! file_get_contents(asset('/svg/user.svg')) !!}</span>
                                <input
                                    type="email"
                                    id="freshfield_name"
                                    name="freshfield_name"
                                    placeholder="Username"
                                    onfocus="handleFocus(event)"
                                    onblur="handleFocus(event)"
                                />
                                <span class="form-item__border"></span>

                            </div>
                        </div>

                        <div class="form__item form-item">
                            <label class="form-item__label" for="freshfield_password">Password</label>
                            <div class="form-item__input" id="freshfield_password_parent">

                                <span class="form-item__icon">{!! file_get_contents(asset('/svg/password.svg')) !!}</span>
                                <input
                                    type="password"
                                    id="freshfield_password"
                                    name="freshfield_password"
                                    placeholder="Password"
                                    onfocus="handleFocus(event)"
                                    onblur="handleFocus(event)"
                                />
                                <span class="form-item__border"></span>

                            </div>
                        </div>

                        <div class="form__item form__item--row form-item">
                            <label class="form-item__label form-item__label--checkbox form-item__checkbox form-item-checkbox" for="freshfield_remember">
                                Remember me
                                <input
                                    type="checkbox"
                                    class="form-item-checkbox__input"
                                    name="freshfield_remember"
                                    id="freshfield_remember"
                                />
                                <span class="form-item-checkbox__mask"></span>
                            </label>
                            <button class="form-item__button">
                                Login
                            </button>
                        </div>

                    </form>

                </div>
            </div>
            <div class="auth__image"></div>
        </div>
    </body>
</html>