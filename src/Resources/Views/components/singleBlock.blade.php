<div ondrop="drop(event)" ondragover="allowDrop(event)">
    <div class="block-list__item block-item" id="{{str_slug($block->name)}}-{{$count}}-parent" draggable="true" ondragstart="drag(event)" data-sort="{{$index + 1}}" data-block-name="{{str_slug($block->name)}}">
        <div class="block-item__head" id="{{str_slug($block->name)}}-{{$count}}-onclick"
             onclick="toggleBlock('{{str_slug($block->name)}}-{{$count}}-body')">
            <div style="display: flex">
                <div class="block-item__head--icon">
                    <i class="{{ $icon }}"></i>
                </div>
                <div class="block-item__head--title" id="{{str_slug($block->name)}}-{{$count}}-title">
                    {{ ucfirst(str_replace('_', ' ', $typeName)) }}
                </div>
            </div>
            <button type="button" onclick="{{str_slug($block->name)}}Block.delete(event)"
                    class="block-item__head--delete"
                    data-info="{{$index}}"><i class="fas fa-trash-alt"></i></button>
        </div>
        <div class="block-item__body block-body" id="{{str_slug($block->name)}}-{{$count}}-body">
            @include('cms::forms.hidden', ['name' => str_slug($block->name).'['.$index.'][type]', 'value' => $value->type, 'id' => str_slug($block->name). "-$index-type"])
            @foreach($blockItems as $key => $blockItem)
                @if(in_array($blockItem, $block->invisible))
                    @include('cms::forms.hidden', ['name' => str_slug($block->name).'['.$index.']['.$blockItem.']', 'value' => isset($value) ? $value->{$blockItem} : "", 'id' => str_slug($block->name).'-$blockItem'])
                @else
                    <div class="blocks-body__item blocks-body-item">
                        <label class="block-body-item__label">{{ !empty($block->labels[$blockItem]) ? $block->labels[$blockItem] : $blockItem }}</label>
                        @include('cms::forms.'. (isset($block->types) && !empty($block->types[$blockItem]) ? $block->types[$blockItem] : 'text'),
                            [
                                'name' => str_slug($block->name).'['.$index.']['.$blockItem.']',
                                'value' => isset($values) ? isset($values->{$blockItem}) ? $values->{$blockItem} : "" : "",
                                'id' => str_slug($block->name)."-$index-$blockItem",
                                'required' => in_array($blockItem, $block->required),
                                'placeholder' => isset($block->labels[$type][$blockItem]) ? $block->labels[$type][$blockItem] : $blockItem
                            ]
                        )
                    </div>
                @endif
            @endforeach
        </div>
        @include('cms::forms.hidden', ['name' => str_slug($block->name).'['.$index.'][prio]', 'value' => ($index + 1), 'id' => str_slug($block->name)."-$index-prio"])
    </div>
</div>