<script>
    let {{str_slug($block->name)}}Block = new Block({
        templates: [
            @foreach($block->blockTypes as $blockName => $blockType)
            {
                type: "{{ $blockName }}",
                template: `@include('cms::components.singleBlockTemplate', ['typeName' => $blockName, 'block' => $block, 'blockItems' => $blockType, 'icon' => isset($block->icons[$blockName]) ? $block->icons[$blockName] : ''])`,
            },
            @endforeach
        ],
        items: {
            @foreach($block->blockTypes as $blockName => $blockType)
                '{{ $blockName }}': [
                    @foreach($blockType as $item)
                        "{{$item}}",
                    @endforeach
                ],
            @endforeach
        },
        name: '{{ str_slug($block->name) }}',
    });
</script>

<div class="block">
    <div class="block__add block-add">
        <button type="button" class="block-add__button" id="blocks-button" onclick="toggleAddBlock('{{ str_slug($block->name) }}-add')"><i class="fas fa-plus"></i></button>
        <div class="block-add__container block-add-container" id="{{ str_slug($block->name) }}-add">
            @foreach($block->blockTypes as $blockName => $blockType)
                <button type="button" class="block-add-container__button" onclick="{{str_slug($block->name)}}Block.add('{{$blockName}}')">
                    <i class="@if(isset($block->icons[$blockName])) {{$block->icons[$blockName]}} @endif"></i>
                    {{ ucfirst(str_replace('_', ' ', $blockName))}}
                </button>
            @endforeach
        </div>
    </div>
    <div class="block__list block-list" id="{{ str_slug($block->name) }}-group">

        @php($count = 0)
        @if (isset($values) && !empty($values->{str_slug($block->name)}))
            @foreach($values->{str_slug($block->name)} as $value)
                @if (!empty($value))
                    @include('cms::components.singleBlock',
                        [
                            'typeName' => $value->type,
                            'block' => $block,
                            'blockItems' => $block->blockTypes[$value->type],
                            'values' => $value,
                            'index' => $count,
                            'icon' => isset($block->icons[$value->type]) ? $block->icons[$value->type] : '',
                        ]
                    )
                    @php($count++)
                @endif
            @endforeach
        @endif
        <script>
            {{str_slug($block->name)}}Block.current = {{ $count > 0 ? $count : 0 }};
        </script>

    </div>

</div>