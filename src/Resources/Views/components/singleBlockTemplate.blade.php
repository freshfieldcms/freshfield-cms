<div ondrop="drop(event)" ondragover="allowDrop(event)">
    <div class="block-list__item block-item" id="BLOCKPLACEHOLDER-parent" draggable="true"
         ondragstart="drag(event)" data-sort="0" data-block-name="{{str_slug($block->name)}}">
        <div class="block-item__head" id="BLOCKPLACEHOLDER-onclick">
            <div style="display: flex">
                <div class="block-item__head--icon">
                    <i class="{{$icon}}"></i>
                </div>
                <div class="block-item__head--title" id="BLOCKPLACEHOLDER-TITLE">
                    {{ucfirst(str_replace('_', ' ', $typeName))}}
                </div>
            </div>
            <button type="button" onclick="{{str_slug($block->name)}}Block.delete(event)"
                    class="block-item__head--delete" id="BLOCKPLACEHOLDER-DELETE" data-info="PLACEHOLDER"><i
                        class="fas fa-trash-alt"></i></button>
        </div>
        <div class="block-item__body block-body" id="BLOCKPLACEHOLDER-BODY">
            @include('cms::forms.hidden', ['name' => str_slug($block->name).'[0][type]', 'value' => '', 'id' => "BLOCKPLACEHOLDER-type"])
            @foreach($blockItems as $key => $blockItem)
                @if(in_array($blockItem, $block->invisible))
                    @include('cms::forms.hidden', ['name' => str_slug($block->name).'[0]['.$blockItem.']', 'value' => "", 'id' => "BLOCKPLACEHOLDER-$blockItem"])
                @else
                    <div class="blocks-body__item blocks-body-item">
                        <label class="block-body-item__label">{{ !empty($block->labels[$blockItem]) ? $block->labels[$blockItem] : $blockItem }}</label>
                        @include('cms::forms.'. (isset($block->types) && !empty($block->types[$blockItem]) ? $block->types[$blockItem] : 'text'),
                            [
                                'name' => str_slug($block->name).'[0]['.$blockItem.']',
                                'value' => "",
                                'id' => "BLOCKPLACEHOLDER-$blockItem",
                                'required' => in_array($blockItem, $block->required),
                                'placeholder' => isset($block->labels) ? $block->labels[$blockItem] : $blockItem
                            ]
                        )
                    </div>
                @endif
            @endforeach
        </div>
    </div>
    @include('cms::forms.hidden', ['name' => str_slug($block->name).'[0][prio]', 'value' => 2, 'id' => 'BLOCKPLACEHOLDER-prio'])
</div>