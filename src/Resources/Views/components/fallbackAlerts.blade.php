@if(!empty(session('success')) && session('success') === true)

    <div class="alert alert--success" id="alert" onload="setTimeout(() => document.getElementById('alert').classList.add('alert--hidden'), 500)">

        <span class="alert__text">
            The operation was successfully finished!
        </span>
        <button type="button" class="alert__button" onclick="document.getElementById('alert').classList.add('alert--hidden')">
            <i class="fas fa-times"></i>
        </button>

    </div>

    <script>
        setTimeout(() => document.getElementById('alert').classList.add('alert--hidden'), 5000)
    </script>

    @php(session()->forget('success'))

@elseif (!empty(session('error')))

    <div class="alert alert--danger" id="alert">

        <span class="alert__text">
            Something went wrong! Please try again
        </span>
        <button type="button" class="alert__button" onclick="document.getElementById('alert').classList.add('alert--hidden')">
            <i class="fas fa-times"></i>
        </button>

    </div>

    <script>
        setTimeout(() => document.getElementById('alert').classList.add('alert--hidden'), 5000)
    </script>

    @php(session()->forget('error'))

@endif