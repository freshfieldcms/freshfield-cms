<h2 class="form-subject__title">
    {{ isset($labels[$title]) ? $labels[$title] : $title }}
</h2>

@foreach($items as $item)
    <div class="form__row form-row @if(gettype($item) !== 'array' && !empty($errors->default->get($item))) form__row--error @endif">
        <div class="form-row__item form-row__item--label form-item">
            <label class="form-item__label" for="{{$title}}.{{$item}}">
                {{ isset($labels[$title.'.'.$item]) ? $labels[$title.'.'.$item] : $item }} @if(in_array($title.'.'.$item, $required))* @endif
            </label>
        </div>
        <div class="form-row__item form-row__item--input form-item">
            @include('cms::forms.'. (isset($types) && !empty($types[$title.'.'.$item]) ? $types[$title.'.'.$item] : 'text'),
                [
                    'name' => $title.'.'.$item,
                    'value' => isset($values) && isset($values->{$title}) && isset($values->{$title}->{$item}) ? $values->{$title}->{$item} : "",
                    'id' => $title.'.'.$item,
                    'required' => in_array($title.'.'.$item, $required),
                    'placeholder' => isset($labels[$title.'.'.$item]) ? $labels[$title.'.'.$item] : $item
                ])
        </div>
    </div>
@endforeach