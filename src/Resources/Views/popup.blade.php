<div class="cover" id="popup">

    <div class="popup">

        <h1 class="popup__title" id="popup-title"></h1>
        <div class="popup__buttons popup-buttons">
            <button class="popup-buttons__button popup-buttons__button--success popup-button" id="first-button"></button>
            <button type="button" class="popup-buttons__button popup-buttons__button--danger popup-button" onclick="document.getElementById('popup').classList.remove('cover--active');">Cancel</button>
        </div>
    </div>

</div>