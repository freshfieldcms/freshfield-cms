function handleFocus(event) {
    const classList = document.getElementById(event.target.id + '_parent').classList;
    if (classList.contains('form-item__input--active')) {
        classList.remove('form-item__input--active');
    } else {
        classList.add('form-item__input--active');
    }
}
