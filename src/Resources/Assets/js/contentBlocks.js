let countBlocks = 0;

function toggleChooseBlock() {
    document.getElementById('blocks-container').classList.toggle('blocks-add__container--active');
}

async function addBlock(type, id) {
    const element = document.getElementById('blocks-'+id);
    let newElement;

    switch(type) {
        case 'title':
            await axios({
                method:'get',
                url:'/api/block/title/'+ ( countBlocks + 1) + '/' + id,
            })
                .then((response) => {
                    countBlocks++;
                    newElement = document.createElement('div');
                    newElement.id = `block-${countBlocks + 1}-${id}`;
                    newElement.innerHTML = response.data;
                    element.appendChild(newElement);
                });
            break;
        case 'text':
            await axios({
                method:'get',
                url:'/api/block/text/'+ ( countBlocks + 1) + '/' + id,
            })
                .then((response) => {
                    countBlocks++;
                    newElement = document.createElement('div');
                    newElement.id = `block-${countBlocks + 1}-${id}`;
                    newElement.innerHTML = response.data;
                    element.appendChild(newElement);
                });
            break;
        case 'image':
            await axios({
                method:'get',
                url:'/api/block/image/'+ ( countBlocks + 1) + '/' + id,
            })
                .then((response) => {
                    countBlocks++;
                    newElement = document.createElement('div');
                    newElement.id = `block-${countBlocks + 1}-${id}`;
                    newElement.innerHTML = response.data;
                    element.appendChild(newElement);
                });
            break;
        case 'video':
            await axios({
                method:'get',
                url:'/api/block/video/'+ ( countBlocks + 1) + '/' + id,
            })
                .then((response) => {
                    countBlocks++;
                    newElement = document.createElement('div');
                    newElement.id = `block-${countBlocks + 1}-${id}`;
                    newElement.innerHTML = response.data;
                    element.appendChild(newElement);
                });
            break;
    }
    tinymce.remove();
    tinymce.init({
        selector: ".tinymce",
        plugins: "code hr wordcount",
        toolbar: "code hr wordcount",
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block : ''
    });

    toggleChooseBlock();
}

function editBlock(id) {
    document.getElementById('form-'+id).classList.toggle('blocks-item__form--active');
    document.getElementById('header-'+id).classList.toggle('blocks-item__header--active');
}

function removeBlock(id) {
    const element = document.getElementById(id);
    element.parentNode.removeChild(element);
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(e) {
    console.log('DRAG', e);
    e.dataTransfer.setData('html', e.target.innerHTML);
    e.dataTransfer.setData('id', e.target.id);
}

function drop(e) {
    console.log('DROP', e);
    document.getElementById(e.dataTransfer.getData('id')).innerHTML = e.target.innerHTML;
    e.target.innerHTML = e.dataTransfer.getData('html');
}