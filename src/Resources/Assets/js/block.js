class Block {
    constructor({templates, name, items}) {
        this.current = 0;
        this.templates = templates;
        this.name = name;
        this.items = items;
    }

    async add(type) {
        const current = this.current;
        const el = document.createElement('div');
        el.innerHTML = this.templates.find(template => template.type === type).template;
        document.getElementById(`${this.name}-group`).appendChild(el);
        const parentElement = document.getElementById('BLOCKPLACEHOLDER-parent');
        parentElement.id = `${this.name}-${current}-parent`;
        parentElement.dataset.sort = current + 1;
        document.getElementById('BLOCKPLACEHOLDER-DELETE').dataset.info = current;
        document.getElementById('BLOCKPLACEHOLDER-DELETE').id = `${this.name}-${current}-delete`;
        document.getElementById('BLOCKPLACEHOLDER-BODY').id = `${this.name}-${current}-body`;

        const onclick = document.getElementById('BLOCKPLACEHOLDER-onclick');
        console.log(`${this.name}-${current}-body`);
        onclick.addEventListener('click', () => toggleBlock(`${this.name}-${current}-body`));
        onclick.id = `${this.name}-${current}-onclick`;

        const typeInput = document.getElementById('BLOCKPLACEHOLDER-type');
        typeInput.id = "";
        typeInput.name = `${this.name}[${current}][type]`;
        typeInput.value = type;

        const prioInput = document.getElementById('BLOCKPLACEHOLDER-prio');
        prioInput.id = `${this.name}-${current}-prio`;
        prioInput.name = `${this.name}[${current}][prio]`;
        prioInput.value = current + 1;

        this.items[type].forEach((item) => {
            const input = document.getElementById(`BLOCKPLACEHOLDER-${item}`);
            input.id = `${this.name}-${item}-${current}`;
            input.name = `${this.name}[${current}][${item}]`;
            if (input.type === 'file') {
                const button = document.getElementById(`BLOCKPLACEHOLDER-${item}-BUTTON`);
                button.onclick = triggerImageUpload;
                button.id = `${this.name}-${item}-${current}-button`;
                button.dataset.targetId = `${this.name}-${item}-${current}`;
            } else if (input.classList.contains('tinymce')) {
                tinymce.init({
                    selector: ".tinymce",
                    plugins: "code hr wordcount",
                    toolbar: "code hr wordcount",
                    force_br_newlines: true,
                    force_p_newlines: false,
                    forced_root_block: ''
                });
            }
        });
        this.current += 1;
    }

    delete(ev) {
        let id;
        if (ev.path[0].tagName === 'I') {
            id = ev.target.parentNode.dataset.info;
        } else {
            id = ev.target.dataset.info;
        }
        document.getElementById(`${this.name}-${id}-parent`).remove();
    }
}