function toggleNavigationDropdown(id) {
    const element = document.getElementById(id);
    element.classList.toggle('navigation__dropdown--active');
}

function toggleNavigation() {
    const element = document.getElementById('navigation');
    element.classList.toggle('navigation__main--active')
}

function onClickAnimation(id) {
    const element = document.getElementById(id);
    if( element.classList.contains('list-mobile-menu__container--active') ||
        element.classList.contains('list-mobile-menu__container--gone'))
    {
        element.classList.toggle('list-mobile-menu__container--active');
        element.classList.toggle('list-mobile-menu__container--gone');
    } else element.classList.add('list-mobile-menu__container--active');
}

function showFilters() {
    const element = document.getElementById('filters');
    if (element) {
        element.classList.toggle('list-filters__container--active');
    }
}

function triggerImageUpload(event) {
    const element = document.getElementById(event.target.dataset.targetId);
    element.click();
}

function replaceOldImage(input) {
    console.log('CHANGING');
    const element = document.getElementById('old_' + input.id);

    const reader = new FileReader();
    reader.onload = function (e) {
        element.src = e.target.result;
    };

    element.src = reader.readAsDataURL(input.files[0]);
}

let toggled = false;
let selectId = null;

function handleClick(e) {
    const containerEl = document.getElementById(selectId).parentElement;
    if (containerEl) {
        if (!containerEl.contains(e.target) && toggled) {
            toggleSelect(selectId);
        }
    }
}

function toggleSelect(id) {
    selectId = id;
    const element = document.getElementById(id);
    if (!toggled) {
        document.addEventListener('click', handleClick, false);
        element.classList.add('form-select__group--active');
        toggled = true;
    } else {
        document.removeEventListener('click', handleClick, false);
        element.classList.remove('form-select__group--active');
        toggled = false;
    }
}

function changeSelect(element) {
    const parent = document.getElementById(element.dataset.parent);
    parent.innerHTML = element.innerText + '<i class="fas fa-caret-down"></i>';
    const input = document.getElementById(parent.dataset.input);
    input.value = element.dataset.value;
    parent.click();
}

function togglePopup(type, onclick) {
    switch(type) {
        case 'confirm':
            const title = document.getElementById('popup-title');
            const firstButton = document.getElementById('first-button');

            title.innerText = 'Are you sure you want to do this?';
            firstButton.addEventListener('click', () => { onclick(); document.getElementById('popup').classList.remove('cover--active'); });
            firstButton.innerText = 'Yes';
            break;
    }

    document.getElementById('popup').classList.add('cover--active');
}

let toggledBlock;

function toggleBlock(block) {
    const element = document.getElementById(block);
    if (toggledBlock === block) {
        document.getElementById(toggledBlock).classList.remove('block-item__body--active');
        toggledBlock = '';
        return;
    } else if (toggledBlock !== '') {
        document.getElementById(toggledBlock).classList.remove('block-item__body--active');
        toggledBlock = '';
    }

    if (!element.classList.contains('block-item__body--active')) {
        toggledBlock = block;
    }
    element.classList.toggle('block-item__body--active');
}

function toggleAddBlock(id) {
    const element = document.getElementById(id);
    if (element) {
        element.classList.toggle('block-add__container--active');
    }
}

/**
 Drag and drop
 */
function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.effectAllowed = "move";
    ev.dataTransfer.setData("html", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    const html = document.getElementById(ev.dataTransfer.getData("html"));
    const sourcePrio = html.dataset.sort;
    const sourceName = html.dataset.blockName;
    console.log(`#${sourceName}-${sourcePrio - 1}-prio`);
    const sourceInput = document.querySelector(`#${sourceName}-${sourcePrio - 1}-prio`);
    console.log(sourceInput, `#${sourceName}-${sourcePrio - 1}-prio`);
    const sourceValue = sourceInput.value;


    const srcParent = html.parentNode;
    const tgt = ev.currentTarget.firstElementChild;
    const targetPrio = tgt.dataset.sort;
    const targetInput = document.querySelector(`#${sourceName}-${targetPrio - 1}-prio`);
    console.log(targetInput, `#${sourceName}-${targetPrio - 1}-prio`);
    const targetValue = targetInput.value;

    targetInput.value = sourceValue;
    sourceInput.value = targetValue;

    ev.currentTarget.replaceChild(html, tgt);
    srcParent.prepend(tgt);
}