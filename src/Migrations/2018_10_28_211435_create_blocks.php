<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks_content', function(Blueprint $table) {
            $table->increments('id');
            $table->string('type')->nullable(false);
            $table->string('title')->nullable(true);
            $table->longText('text')->nullable(true);
            $table->unsignedInteger('image')->default(1);
            $table->string('video_url')->nullable(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blocks_content');
    }
}
