<?php

return [

    /**
     * Here you link your forms
     */
    'forms' => [
        // CORE     \/
        'users' => \Freshfield\Core\Modules\Users\Forms\UsersForm::class,
        'cms_routes' => \Freshfield\Core\Modules\CmsRoutes\Forms\CmsRoutesForm::class,
        'images' => \Freshfield\Core\Modules\Images\Forms\ImagesForm::class,
        'profile' => \Freshfield\Core\Modules\Profile\Forms\ProfileForm::class,
        'cms_roles' => \Freshfield\Core\Modules\Roles\Forms\RolesForm::class,

        // CUSTOM   \/

    ],

    /**
     * Here you link your lists
     */
    'lists' => [
        // CORE     \/
        'users' => \Freshfield\Core\Modules\Users\Lists\UsersList::class,

        // CUSTOM   \/
    ],

    /**
     * Here you link your blocks
     */
    'blocks' => [
        // CORE     \/
        'content' => \Freshfield\Core\Blocks\Content\ContentBlock::class,

        // CUSTOM   \/
    ],

    /**
     * Here you link your Commands
     */
    'commands' => [
        // CORE     \/
        'initialize' => \Freshfield\Core\Commands\Initialize::class,

        // CUSTOM   \/
    ]

];