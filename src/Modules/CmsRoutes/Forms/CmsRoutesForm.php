<?php

namespace Freshfield\Core\Modules\CmsRoutes\Forms;


use Freshfield\Core\Base\Form\BaseForm;

class CmsRoutesForm extends BaseForm
{
    public function getTable()
    {
        return 'cms_routes';
    }

    public function getTitle()
    {
        return 'CMS Routes';
    }

    public function getItems()
    {
        return [
            'id',
            'name',
            'url',
            'icon',
            'parent_id',
        ];
    }

    public function getLabels()
    {
        return [
            'name' => 'Title',
            'url' => 'URL',
            'icon' => 'Icon',
            'parent_id' => 'Parent'
        ];
    }

    public function getVisible()
    {
        return [
            'name',
            'url',
            'icon',
            'parent_id'
        ];
    }

    public function getInvisible()
    {
        return [
            'id'
        ];
    }

    public function getForeign()
    {
        return [
            'parent_id' => 'cms_routes.id.name'
        ];
    }

    public function getRequired()
    {
        return [
            'name',
            'url'
        ];
    }
}