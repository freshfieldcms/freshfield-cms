<?php

namespace Freshfield\Core\Modules\Users\Forms;

use Freshfield\Core\Base\Form\BaseForm;

class UsersForm extends BaseForm
{
    public function getTitle()
    {
        return 'User';
    }

    public function getItems()
    {
        return [
            'id',
            'name',
            'email',
            'role_id',
            'password'
        ];
    }

    public function getTypes()
    {
        return [
            'id' => 'disabled',
            'email' => 'email',
            'password' => 'password',
            'role_id' => 'select'
        ];
    }

    public function getLabels()
    {
        return [
            'name' => 'Name',
            'role_id' => 'Role',
            'email' => 'Email',
            'password' => 'Password'
        ];
    }

    public function getVisible()
    {
        return [
            'name',
            'role_id',
            'email'
        ];
    }

    public function getTable()
    {
        return 'users';
    }

    public function getForeign()
    {
        return [
            'role_id' => 'cms_roles.id.name'
        ];
    }

    public function getValidation()
    {
        return [
            'password' => 'nullable | confirmed',
            'email' => 'required | email | required',
            'name' => 'required',
            'role_id' => 'required',
        ];
    }

    public function getRequired()
    {
        return [
            'name',
            'email',
        ];
    }

}