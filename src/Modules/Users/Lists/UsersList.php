<?php

namespace Freshfield\Core\Modules\Users\Lists;

use Freshfield\Core\Base\Lists\BaseList;

class UsersList extends BaseList
{
    public function getTitle()
    {
        return 'Users';
    }

    public function getTable()
    {
        return 'users';
    }

    public function getForeignKeys()
    {
        return [];
    }

    public function getVisible()
    {
        return [
            'name',
            'email',
            'role_id'
        ];
    }

    public function getInvisible()
    {
        return [
            'id'
        ];
    }

    public function getLabels()
    {
        return [
            'name' => 'Name',
            'email' => 'Email',
            'role_id' => 'Role',
        ];
    }

    public function getIndex()
    {
        return [
            'id'
        ];
    }

    public function getSort()
    {
        return [
            'id DESC',
        ];
    }

    public function getFilter()
    {
        return [
            'role_id'
        ];
    }

    public function getMobileVisible()
    {
        return [
            'email'
        ];
    }
}