<?php

namespace Freshfield\Core\Modules\Images\Forms;

use Freshfield\Core\Base\Form\BaseForm;

class ImagesForm extends BaseForm
{
    public function getTitle()
    {
        return 'Images';
    }

    public function getItems()
    {
        return [
            'id',
            'source'
        ];
    }

    public function getLabels()
    {
        return [
            'source' => 'Source'
        ];
    }

    public function getVisible()
    {
        return [
            'source'
        ];
    }

    public function getInvisible()
    {
        return [
            'id'
        ];
    }

    public function getTable()
    {
        return 'images';
    }

}