<?php

namespace Freshfield\Core\Modules\Profile\Forms;

use Freshfield\Core\Base\Form\BaseForm;

class ProfileForm extends BaseForm
{
    public function getTitle()
    {
        return 'Profile';
    }

    public function getItems()
    {
        return [
            'id',
            'name',
            'email',
            'password'
        ];
    }

    public function getTypes()
    {
        return [
            'id' => 'hidden',
            'email' => 'email',
            'password' => 'password',
        ];
    }

    public function getLabels()
    {
        return [
            'name' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
        ];
    }

    public function getVisible()
    {
        return [
            'id',
            'name',
            'email',
            'password',
        ];
    }

    public function getTable()
    {
        return 'users';
    }

}