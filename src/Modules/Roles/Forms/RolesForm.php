<?php

namespace Freshfield\Core\Modules\Roles\Forms;

use Freshfield\Core\Base\Form\BaseForm;

class RolesForm extends BaseForm
{
    public function getTitle()
    {
        return 'User roles';
    }

    public function getItems()
    {
        return [
            'id',
            'name',
            'permissions'
        ];
    }

    public function getLabels()
    {
        return [
            'name' => 'Name',
            'permissions' => 'Permissions'
        ];
    }

    public function getVisible()
    {
        return [
            'name',
            'permissions'
        ];
    }

    public function getInvisible()
    {
        return [
            'id'
        ];
    }

    public function getTable()
    {
        return 'cms_roles';
    }

}