<?php

namespace Freshfield\Core\Render;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BaseRender
{

    /**
     * @return array
     */
    public function getRoutes()
    {
        $id = Auth::id();

        $query = DB::table('users')
            ->join('cms_roles', 'users.role_id', '=', 'cms_roles.id')
            ->where('users.id', $id)
            ->select('cms_roles.permissions')
            ->first();

        if($query->permissions === '*') {
            $output = $this->__getAllRoutes();
            return $this->__sortRoutes($output);
        } else {
            $routes = explode(',', $query->permissions);
            $output = $this->__getSpecificRoutes($routes);
            return $this->__sortRoutes($output);
        }
    }

    /**
     * @return array
     */
    private function __getAllRoutes()
    {
        return DB::table('cms_routes')->select('id', 'name', 'url', 'parent_id')->get()->toArray();
    }

    private function __getSpecificRoutes($routes) {
        $output = [];
        foreach($routes as $route) {
            $output[] = DB::table('cms_routes')->select('id', 'name', 'url', 'parent_id')->where('id', $route)->first();
        }
        return $output;
    }

    /**
     * @param array $routes
     * @return array
     */
    private function __sortRoutes(array $routes)
    {
        if(empty($routes[0])) return [];
        $output = [];
        foreach($routes as $route) {
            if(!empty($route->parent_id)) {
                if(isset($output[$route->parent_id])) {
                    $output[$route->parent_id]['childs'][] = $route;
                } else {
                    $output[$route->id] = [
                        'main' => $route,
                        'childs' => []
                    ];
                }
            } else {
                $output[$route->id] = [
                    'main' => $route,
                    'childs' => []
                ];
            }
        }
        return $output;
    }
}