<?php

namespace Freshfield\Core\Http\Middleware;


use Closure;
use Freshfield\Core\Auth\Access;
use Illuminate\Http\Request;

class CheckAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        Access::check($request->path());
        return $next($request);
    }
}