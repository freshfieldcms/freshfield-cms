<?php

namespace Freshfield\Core\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class CheckScheduledCommands
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $commands = DB::table('scheduled_commands')->select('*')->get();
        foreach($commands as $command) {
            $time = date('Y-m-d', strtotime('-'.$command->run_every.' days'));
            if (strtotime($time) > strtotime($command->last_run)) {
                Artisan::call($command->command);
                DB::table('scheduled_commands')->where('id', $command->id)->update(['last_run' => date('Y-m-d')]);
            }
        }

        return $next($request);
    }
}
