<?php

namespace Freshfield\Core\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

/**
 * Get the path the user should be redirected to when they are not authenticated.
 *
 * @param  \Illuminate\Http\Request  $request
 * @return string
 */
class CheckAuthentication extends Middleware
{
    protected function redirectTo($request)
    {
        return route('login');
    }
}
