<?php

namespace Freshfield\Core\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * @param Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');
        $credentials['deleted_at'] = null;

        return $credentials;
    }

    public function index()
    {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        }
        return view('cms::auth.login');
    }

    public function login(Request $request)
    {
        if ($request->has('freshfield_name') && $request->has('freshfield_password')) {
            if (Auth::attempt(['email' => $request->get('freshfield_name'), 'password' => $request->get('freshfield_password')]))
            {
                if (Auth::user()->deleted_at === null) {
                    return redirect()->intended();
                }
                session(['login-error' => 'User not found']);
                Auth::logout();
            } else {
                session(['login-error' => 'User not found']);
            }
        }
        return redirect()->route('login');
    }
}
