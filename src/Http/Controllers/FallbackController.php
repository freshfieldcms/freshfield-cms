<?php

namespace Freshfield\Core\Http\Controllers;


use Freshfield\Core\Render\BaseRender;

class FallbackController extends BaseRender
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function success() {
        return view('cms::fallback', ['type' => 'success', 'routes' => $this->getRoutes()]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function error()
    {
        return view('cms::fallback', ['type' => 'error', 'routes' => $this->getRoutes()]);
    }
}