<?php

namespace Freshfield\Core\Http\Controllers;

use Freshfield\Core\Builders\ApiBuilder;
use Illuminate\Http\Request;

class ApiController
{
    protected $privateForms = [
        'users',
        'cms-routes',
        'cms-roles',
    ];

    /**
     * @param Request $request
     * @param $type
     * @param $id
     * @return null
     */
    public function get(Request $request, $type, $id = null)
    {
        if (!ApiBuilder::validateKey($request->get('key'))) return response()->json(['message' => "Bad request: API key is not correct or missing"]);
        if (in_array($type, $this->privateForms)) return response()->json(['message' => "Bad request: Type not found"]);
        return response()->json((new ApiBuilder($type, $id))->get());
    }
}