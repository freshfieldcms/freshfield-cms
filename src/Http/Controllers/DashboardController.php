<?php

namespace Freshfield\Core\Http\Controllers;


use Freshfield\Core\Render\BaseRender;

class DashboardController extends BaseRender
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('cms::dashboard', ['routes' => $this->getRoutes()]);
    }
}