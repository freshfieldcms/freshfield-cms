<?php

namespace Freshfield\Core\Http\Controllers;

use Freshfield\Core\Builders\FormBuilder;
use Freshfield\Core\Factories\ModelFactory;
use Freshfield\Core\Processing\Retrieve;
use Freshfield\Core\Processing\Save;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileController
{
    /**
     * @var FormBuilder
     */
    protected $formBuilder;

    /**
     * ProfileController constructor.
     * @param FormBuilder $formBuilder
     */
    public function __construct(FormBuilder $formBuilder)
    {
        $this->formBuilder = $formBuilder;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
//        $model = ModelFactory::create('profile');
        if(Auth::check()) {
            return $this->formBuilder->render('profile', Auth::id());
        } return redirect()->route('login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function save(Request $request)
    {
        $form = Retrieve::form('profile');

        $insert = [];
        $types = $form->getTypes();
        $items = $form->getItems();

        $time = date('Y-m-d H:i:s');

        foreach ($items as $item) {
            if (!$request->has($item)) continue;

            if (!empty($types[$item]) && !empty($request->get($item))) {
                if ($types[$item] === 'password') {
                    $insert[$item] = Hash::make($request->get($item));
                } else if ($types[$item] === 'image') {
                    $insert[$item] = Save::image($request->file($item));
                } else {
                    $insert[$item] = $request->get($item);
                }
            } else {
                $insert[$item] = $request->get($item);
            }
        }

        if (empty($request->get('id'))) {
            $insert['created_at'] = $time;
        }
        $insert['updated_at'] = $time;

        $insert = array_filter($insert);

        DB::table($form->getTable())->where('id', $request->get('id'))->update($insert);

        if (isset($insert['password'])) {
            session(['message' => 'Please login with your new password!']);
        } else {
            session(['success' => true]);
        }

        return redirect()->route('dashboard');
    }
}
