<?php

namespace Freshfield\Core\Http\Controllers;

use Freshfield\Core\Builders\FormBuilder;
use Freshfield\Core\Builders\ListBuilder;
use Freshfield\Core\Builders\SettingsBuilder;
use Illuminate\Http\Request;

class GeneralController
{
    protected $formBuilder;
    protected $listBuilder;
    protected $settingsBuilder;

    /**
     * GeneralController constructor.
     * @param FormBuilder $formBuilder
     * @param ListBuilder $listBuilder
     * @param SettingsBuilder $settingsBuilder
     */
    public function __construct(FormBuilder $formBuilder, ListBuilder $listBuilder, SettingsBuilder $settingsBuilder)
    {
        $this->formBuilder = $formBuilder;
        $this->listBuilder = $listBuilder;
        $this->settingsBuilder = $settingsBuilder;
    }

    /**
     * @param string $type
     * @param string $filter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index(string $type, string $filter = null)
    {
        return $this->listBuilder->render($type, $filter);
    }

    /**
     * @param string $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create(string $type)
    {
        return $this->formBuilder->render($type);
    }

    /**
     * @param string $type
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function edit(string $type, $id)
    {
        return $this->formBuilder->render($type, $id);
    }

    /**
     * @param string $type
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function save(string $type, Request $request)
    {
        return $this->formBuilder->save($request, $type);
    }

    /**
     * @param string $type
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function delete(string $type, $id)
    {
        return $this->formBuilder->delete($type, $id);
    }

    /**
     * @param string $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function trash(string $type)
    {
        return $this->listBuilder->trash($type);
    }

    /**
     * @param string $type
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function restore(string $type, $id)
    {
        return $this->formBuilder->restore($type, $id);
    }

    public function settings()
    {
        return $this->settingsBuilder->index();
    }
}