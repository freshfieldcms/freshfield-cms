<?php

namespace Freshfield\Core\Commands;

use Illuminate\Console\Command;

class Initialize extends Command
{
    protected $signature = 'freshfield:init';

    protected $description = 'Initialize project';

    // @TODO .env dingen vragen
    protected $commands = [
        'freshfield:auth',
    ];

    public function handle()
    {
        if ($this->confirm('Are you sure you want to do this? This will delete some of the basic configuration of Laravel.')) {
            $bar = $this->output->createProgressBar(count($this->commands));
            $bar->start();

            foreach($this->commands as $command) {
                $this->call($command);
                $bar->advance();
            }

            $bar->finish();
        }
    }
}