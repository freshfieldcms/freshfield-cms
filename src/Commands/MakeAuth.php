<?php

namespace Freshfield\Core\Commands;


use Illuminate\Console\Command;

class MakeAuth extends Command
{
    protected $signature = 'freshfield:auth';

    // @TODO Better description
    protected $description = 'Does everything so Auth works';

    protected function __emptyItems() {
        exec('echo "" > ' . storage_path('routes/web.php'));
        exec('echo "" > ' . storage_path('routes/api.php'));
        exec('rm -rf' . resource_path('views/auth/'));
        $this->comment('Routes have been cleared!');
    }

    public function handler()
    {
        $this->call('auth:make');
        $this->__emptyItems();
    }
}